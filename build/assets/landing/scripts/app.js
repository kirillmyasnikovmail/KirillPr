(function () {
    "use strict";

    /**
     * Init Swiper slider
     *
     * @param sliderClass
     * @param options
     */
    function initSlider(sliderClass, options) {
        var slider = document.querySelector(sliderClass);

        if (slider && typeof Swiper === 'function') {
            new Swiper(slider, options || {});
        }
    }
    $('.carousel__container').flipster({
        style: 'flat',
        loop: true,
    });
    /**
     * Init 3d carousel
     */
    function initFlipster() {
        var $buttonNext = $('.carousel__next');
        var $buttonPrev = $('.carousel__prev');
        var $flipster = null;
        var $logoPager = $('.logos');
        var $logoPagerLinks = $logoPager.find('a');
        var $pager = $('.carousel__dots');
        var $pagerLinks = null;
        var $slider = $('.carousel__container');
        var $slides = $slider.find('li');

        function addSlidesIndexes() {
            $slides.each(function (i) {
                $(this).attr('data-slide', i);
            });
        }

        function initFlipster() {
            $flipster = $slider.flipster({
                style: 'flat',
                loop: true,
                onItemSwitch: function (curr, prev) {
                    setPagerActive($(curr).attr('data-slide'));
                }
            });
        }

        function buildPager() {
            var $dots = '';
            $slides.each(function (i) {
                $dots += '<a data-slide="' + i + '" href="#"></a>';
            });
            $pager.append($dots);
            $pagerLinks = $pager.find('a');
        }

        function setPagerActive(active) {
            var activeIndex = active || $slider.find('.flipster__item--current').attr('data-slide');
            $pager.find('a').removeClass('active');
            $logoPagerLinks.removeClass('active');
            $pager.find("[data-slide=" + activeIndex + "]").addClass('active');
            $logoPager.find("[data-slide=" + activeIndex + "]").addClass('active');
        }

        function initButtons() {
            if ($buttonNext.length) {
                $buttonNext.on('click', function (e) {
                    e.preventDefault();
                    $flipster.flipster('next');
                    setPagerActive();
                });
            }

            if ($buttonPrev.length) {
                $buttonPrev.on('click', function (e) {
                    e.preventDefault();
                    $flipster.flipster('prev');
                    setPagerActive();
                });
            }

            $pagerLinks.each(function () {
                $(this).on('click', function (e) {
                    e.preventDefault();
                    var slideIndex = $(this).attr('data-slide');
                    $flipster.flipster('jump', parseInt(slideIndex));
                    setPagerActive();
                });
            });

            $logoPagerLinks.each(function () {
                $(this).on('click', function (e) {
                    e.preventDefault();
                    var slideIndex = $(this).attr('data-slide');
                    $flipster.flipster('jump', parseInt(slideIndex));
                    setPagerActive();
                });
            });
        }

        if ($slider.length) {
            addSlidesIndexes();
            initFlipster();
            buildPager();
            initButtons();
            setPagerActive();
        }
    }

    function changeSliderDescription(swiper) {
        var activeIndex = swiper.activeIndex;
        var activeSlide = swiper.slides[activeIndex];
        var activeSlideText = $(activeSlide).find('.slider__text').text();
        $('.slider__description').text(activeSlideText);
    }

    // Window is loaded
    window.addEventListener('load', function () {
        initSlider('.slider__container', {
            pagination: '.slider__swiper .slider__pagination',
            nextButton: '.slider__swiper .slider__next',
            prevButton: '.slider__swiper .slider__prev',
            paginationClickable: true,
            loop: true,
            onInit: function (swiper) {
                changeSliderDescription(swiper);
            },
            onSlideChangeEnd: function (swiper) {
                changeSliderDescription(swiper);
            }
        });

        initFlipster();
    });

    if(typeof chrome != "undefined" && chrome.runtime){
        var self = this;
        chrome.runtime.sendMessage("hpkajobmmfgnmmljdoajmkmohgjcanan", {message: "version"}, function (reply) {
            if(reply){
                $("#ext_banner").hide();
            }
            console.log(reply);
        });
    }


}());
