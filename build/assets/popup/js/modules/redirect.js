/**
 * Created by diman on 06.12.16.
 */


var Redirect = (function () {
    var totalTimeCounter = 1;
    var errorTimeCounter = 1;
    setInterval(function () {
        totalTimeCounter++;
        errorTimeCounter++;
    }, 1000);


    function showError(msg) {
        document.getElementById('error').innerHTML = msg;
    }
    function hideError() {
        document.getElementById('error').innerHTML = "";
    }

    /*
     send request until maxExecutionTime in seconds have passed
     url - url address of request
     timeInterval - interval after which send request
     errorTime - time after which show user errors
     maxExecutionTime - total time after which stop making request
     */
    function sendRequest(url, timeInterval, errorTime, maxExecutionTime) {
        hideError();
        xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function(){
            if (xhr.readyState == 4){
                if (xhr.status == 200){
                    response = JSON.parse(xhr.responseText);
                    console.log(xhr.responseText, response.partnerUrl);
                    window.location = response.partnerUrl;
                }
                else{
                    console.log("fail " + totalTimeCounter);
                    if(errorTimeCounter - errorTime > 0){
                        errorTimeCounter = errorTimeCounter - errorTime;
                        showError("Мы решаем вашу проблему. Пожалуйста дождитесь ответа");
                    }
                    if(maxExecutionTime > totalTimeCounter){
                        setTimeout(function () {
                            sendRequest(url, timeInterval, errorTime, maxExecutionTime)
                        }, timeInterval * 1000)
                    }
                    else{
                        showError("Переход в магазин временно недоступен. Попробуйте позже.");
                    }
                }
            }
        }
        xhr.open('GET', url, true)
        xhr.send()
    }


    function simpleRequest(url) {
        xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function(){
            if (xhr.readyState == 4){
                if (xhr.status == 200){
                    document.getElementById("redirect_btn").removeAttribute("disabled");
                    response = JSON.parse(xhr.responseText);
                    window.location = response.partnerUrl;
                }
                else{
                    showError("Переход в магазин временно недоступен. Попробуйте позже.");
                }
            }
        }
        xhr.open('GET', url, true);
        xhr.send();
    }

    function replaceUserId(url) {
        var urlParts = url.split("?"),
            params = urlParts[1].split("&");
        for(var i = 0, len = params.length; i < len; i++){
            var item = params[i].split("=");
            if(item[0].toLowerCase() == "userid"){
                item[1]
            }
        }
    }

    var obj = {}
    obj.start = function (userId) {

        var url = "https://payqr.ru/websdk/api/partners/resolvedlink" + window.location.search;
        if(userId){
            url += "&userId=" + userId;
        }

        document.getElementById("redirect_btn").addEventListener("click", function () {
            document.getElementById("redirect_btn").setAttribute("disabled","disabled");
            simpleRequest(url);
        });
        
        setTimeout(function () {
            sendRequest(url, 1, 3, 10);
        }, 1000);
    }
    
    return obj;

})()




