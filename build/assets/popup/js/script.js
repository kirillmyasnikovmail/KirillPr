/**
 * Created by diman on 26.04.17.
 * User anonymus register page
 */


window.addEventListener("load", function () {
    document.querySelector("#proc").innerHTML = Helper.getParameterByName("proc");
    document.querySelector("#companyName").innerHTML = Helper.getParameterByName("companyName");
    
    var userId = Helper.getParameterByName("userId") || Helper.getCookie("UserId"),
        partnerId = Helper.getParameterByName("partnerId");
    console.log(userId, partnerId);
    //check if userId exist and not set like template &userId={userId}
    if(userId && userId.substr(0,1) != "{"){
        Redirect.start(userId);
    }
    else if(partnerId){
        var url = "https://www.workle.ru/promopage/workle-bonus/?path=partner/" + partnerId;
        window.location = url;
    }
    else{
        var url = "https://www.workle.ru/";
        window.location = url;
    }
});

