/**
 * Created by diman on 29.05.17.
 */


document.addEventListener("DOMContentLoaded", function(event) {
    const iframe = document.getElementById("worklebonusclub");
    window.addEventListener("message", function(event){
        if(event.origin == "https://worklebonusclub.popup.payqr.ru"){
            console.log("get message from worklebonusclub", event.data);
            if(event.data.height){
                iframe.height = parseInt(event.data.height) + 4;
            }
            if(event.data.top){
                console.log('скролл вверх')
                window.scrollTo(0, 0)
            }
            if(event.data.bcUserId){
                createCookie("bcUserId", event.data.bcUserId);
            }
        }
    });
    if(getQueryVariable("path")){
        setTimeout(function () {
            iframe.src = "https://worklebonusclub.popup.payqr.ru/" + getQueryVariable("path");
        }, 1000);
    }
});

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return false;
}
function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}