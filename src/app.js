/**
 * Created by diman on 25.04.17.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import Cookies from 'universal-cookie';
import axios from 'axios';

import config from 'site-config';
import Router from './router';
import store from "./redux/store"
import * as webAPI from "./api/webapi";

const cookies = new Cookies();



//axios config
axios.defaults.baseURL = config.baseURL;
axios.defaults.timeout = 20000;
axios.defaults.headers.common['X-PlatformId'] = config.platformId;

var userId = cookies.get(config.cookieUserIdName),
    accessToken = cookies.get(config.cookieAuthTokenName);
if(userId){
    axios.defaults.headers.common["UserId"] = userId;
}
if(accessToken){
    axios.defaults.headers.common["PayQRApiAuthorization"] = accessToken;
}

window.onload = () => {
    webAPI.authUser();
    webAPI.getProfile();
    webAPI.getCatalog();

    ReactDOM.render(
        <Provider store={store}>
            <Router />
        </Provider>,
        document.getElementById('main'));
};


/*
 my custom HMR simulation
 todo: rewrite to https://github.com/gaearon/react-hot-loader
 */
var sendevent = require('sendevent');
if(process.env.NODE_ENV != "production"){
    sendevent('/eventstream', function(event) {
        if(event.reload){
            window.location.reload();
        }
    });
}