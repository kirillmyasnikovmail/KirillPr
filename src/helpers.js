/**
 * Created by diman on 07.06.17.
 */


import config from 'site-config';
import Cookies from 'universal-cookie';


const cookies = new Cookies();

export function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


export function getQueryVariable(variable){
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return false;
}

export function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}



function replaceSpecialChars(word) {
    var chars = {
        " ": "-",
        ".": "",
        "'": "",
        '"': "",
        "!": "",
        "№":"",
        "&": "i",
        "«":"",
        "»":""
    };
    return word.split('').map(function (char) {
        return chars[char] != undefined ? chars[char] : char;
    }).join("");
}

function convertToCyrillic(word) {
    var chars = {
        a: "а",
        b: "б",
        c: "с",
        d: "д",
        e: "е",
        f: "ф",
        g: "г",
        h: "х",
        i: "и",
        j: "ж",
        k: "к",
        l: "л",
        m: "м",
        n: "н",
        o: "о",
        p: "п",
        q: "к",
        r: "р",
        s: "с",
        t: "т",
        u: "у",
        v: "в",
        w: "в",
        x: "кс",
        y: "ай",
        z: "з",
    };
    return word.split('').map(function (char) {
        return chars[char] != undefined ? chars[char] : char;
    }).join("");
}
function convertToLatin(word) {
    var chars = {
        'а': 'a',
        'б': 'b',
        'в': 'v',
        'г': 'g',
        'д': 'd',
        'е': 'e',
        'ё': 'yo',
        'ж': 'zh',
        'з': 'z',
        'и': 'i',
        'й': 'j',
        'к': 'k',
        'л': 'l',
        'м': 'm',
        'н': 'n',
        'о': 'o',
        'п': 'p',
        'р': 'r',
        'с': 's',
        'т': 't',
        'у': 'u',
        'ф': 'f',
        'х': 'h',
        'ц': 'c',
        'ч': 'ch',
        'ш': 'sh',
        'щ': 'sh',
        'ъ': '',
        'ы': 'y',
        'ь': '',
        'э': 'e',
        'ю': 'yu',
        'я': 'ya'
    };
    return word.split('').map(function (char) {
        return chars[char] != undefined ? chars[char] : char;
    }).join("");
}


export function getSemanticLatinName(name) {
    return convertToLatin(replaceSpecialChars(name.trim().toLowerCase()));
}
export function getSemanticCyrillic(name) {
    return convertToCyrillic(replaceSpecialChars(name.trim().toLowerCase()));
}


export function getPartnerExternalUrl(shop) {
    const params = {
        userId: cookies.get("userId"),
        platformId: config.platformId,
        companyName: shop.name,
        proc: shop.promoActions[0].amount.value + (shop.promoActions[0].amount.type == "%" ? shop.promoActions[0].amount.type : ""),
    }
    const query = Object.keys(params).map(i => i + "=" + params[i]).join("&");
    const shopLink = "https://payqr.ru/mobile/api/1.0/partners/link/"+shop.id+"?"+query;
    return shopLink;
}

export function formatIsoDate(date) {
    date = new Date(date);
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    var hours = date.getHours().toString();
    hours = hours.length > 1 ? hours : '0' + hours;

    var minutes = date.getMinutes().toString();
    minutes = minutes.length > 1 ? minutes : '0' + minutes;

    var newDate = day + "-" + month + "-" +  year + " " + hours + ":" + minutes;
    return newDate;
}

export function clearExceptDigits(str) {
  return str.replace(/[^0-9.]/g, "");
}