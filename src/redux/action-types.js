/**
 * Created by diman on 01.06.17.
 */


export const GET_PROFILE_SUCCESS = "GET_PROFILE_SUCCESS";
export const UPDATE_PROFILE_SUCCESS = "UPDATE_PROFILE_SUCCESS";

export const GET_CATALOG_SUCCESS = "GET_CATALOG_SUCCESS";
export const GET_OPERATIONS_SUCCESS = "GET_OPERATIONS_SUCCESS";

export const POST_FEEDBACK_SUCCESS = "POST_FEEDBACK_SUCCESS";

export const CHANGE_SORT_TYPE = 'CHANGE_SORT_TYPE'
export const CHANGE_SEARCH_TEXT = 'CHANGE_SEARCH_TEXT'