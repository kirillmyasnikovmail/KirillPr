/**
 * Created by diman on 05.06.17.
 */


import * as types from '../action-types';
import * as helpers from "../../helpers";

const initialState = {
    data: {},
    categories: [],
    sort: 'popular',
    searchText: ''
};

const catalogReducer = function(state = initialState, action) {

    switch(action.type) {

        case types.GET_CATALOG_SUCCESS:
            const catalog = Object.assign({}, state, { data: action.data });
            const usedCategory = []
            let categories = []

            catalog.data.map(item => {
                item._partnerLink = "/partners/" + helpers.getSemanticLatinName(item.name);
                item._partnerExternalLink = helpers.getPartnerExternalUrl(item);
                item._src = "https://payqr.ru" + item.brand.imageUrl;
                item.category._name = helpers.getSemanticLatinName(item.category.name);

              if (!usedCategory.includes(item.category.name)) {
                  usedCategory.push(item.category.name)
                  categories.push(Object.assign({}, item.category, { count: 1 }))
              } else {
                  categories = categories.map(element => element.name === item.category.name ? Object.assign({}, element, { count: element.count + 1 }) : element)
              }
            });
            return Object.assign({}, state, { data: catalog.data, categories: categories });

        case types.CHANGE_SORT_TYPE:
            return Object.assign({}, state, { sort: action.payload })

        case types.CHANGE_SEARCH_TEXT:
            return Object.assign({}, state, { searchText: action.payload })

    }

    return state;

}

export default catalogReducer;
