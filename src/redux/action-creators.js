/**
 * Created by diman on 05.06.17.
 */

import * as types from "./action-types";


export function getProfileSuccess(data) {
    return{
        type: types.GET_PROFILE_SUCCESS,
        data
    }
}

export function getCatalogSuccess(data) {
    return{
        type: types.GET_CATALOG_SUCCESS,
        data
    }
}

export function getOperationsSuccess(data) {
    return{
        type: types.GET_OPERATIONS_SUCCESS,
        data
    }
}

export function postFeedbackSuccess(data) {
    return{
        type: types.POST_FEEDBACK_SUCCESS,
        data
    }
}

// Изменение типа сортировки
export function changeSortType (type) {
    return {
        type: types.CHANGE_SORT_TYPE,
        payload: type
    }
}

export function changeSearchText (text) {
    return {
        type: types.CHANGE_SEARCH_TEXT,
        payload: text
    }
}
