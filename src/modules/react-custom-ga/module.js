/**
 * Created by diman on 26.06.17.
 */

/*
 https://web-design-weekly.com/2016/07/08/adding-google-analytics-react-application/
 */


import React from 'react';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-53583170-19');

import clickEvents from './click-events';
import changeEvents from './change-events';

const ReactCustomGA = {
    Click: (name, prev, category, action, label, value) => {
        var event = clickEvents[name];
        if (event) {
            if (event[prev]) {
                event = event[prev];
            }
            if (event.default) {
                event = event.default;
            }

            if (category) {
                event.category = category;
            }
            if (label) {
                event.label = label;
            }
            if (action) {
                event.action = action;

            }
            if (value) {
                event.value = value;
            }
            console.log(event);
            ReactGA.event(event);
        }
    },

    Change: (name, prev, category, action, label, value) => {
        var event = changeEvents[name];
        if (event) {

            if (event[prev]) {
                event = event[prev];
            }
            if (event.default) {
                event = event.default;
            }

            if (category) {
                event.category = category;
            }
            if (label) {
                event.label = label;
            }
            if (action) {
                event.action = action;

            }
            if (value) {
                event.value = value;
            }
            console.log(event);
            ReactGA.event(event);
        }
    },

    Load: (category, action, label) => {
        const event = {
            category: category,
            action: action,
            label: label,
        };
        console.log(event);
        ReactGA.event(event);
    },


    ChangeShop: (name) => {
        const event = {
            category: "item",
            action: "open",
            label: name,
        };
        console.log(event);
        ReactGA.event(event);
    }


}

export default ReactCustomGA;