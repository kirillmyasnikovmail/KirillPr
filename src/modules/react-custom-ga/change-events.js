const PathnameChangeEvents = {
    "/partners/popular":{
        category: "page",
        action: "open",
        label: "main"
    },
    "/profile":{
        category: "page",
        action: "open",
        label: "user"
    },
    "/profile/history/purchase":{
        category: "page",
        action: "open",
        label: "user"
    },
    "/profile/withdraw":{
        category: "page",
        action: "open",
        label: "withdraw"
    },
    "/feedback":{
        category: "page",
        action: "open",
        label: "feedback"
    },
    "/faq":{
        category: "page",
        action: "open",
        label: "faq_page"
    },
}


export default PathnameChangeEvents;