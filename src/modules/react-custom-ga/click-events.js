const clickEvents = {
    faq:{
        category: "button",
        action: "click",
        label: "faq"
    },
    search:{
        category: "button",
        action: "click",
        label: "search"
    },
}


export default clickEvents;