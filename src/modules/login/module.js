'use strict'


import React from 'react';
import axios from 'axios';
import Cookies from 'universal-cookie';



var cookies = new Cookies();

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            errors: [],
            login: "",
            password: "",
            step: "register"
        }
        this.login = this.login.bind(this);
        this.stateChange = this.stateChange.bind(this);
    }


    stateChange(e){
        const state = {};
        state[e.target.name] = e.target.value;
        this.setState(state)
    }

    login(){
        const errors = [];
        if(!this.state.login){
            errors.push("Введите логин");
        }
        if(!this.state.password){
            errors.push("Введите пароль");
        }
        if(errors.length > 0){
            this.setState({errors: errors});
            return false;
        }

        axios.post("users/login", {
            login: this.state.login,
            password: this.state.password
        }).then(response=>{
            console.log(response)
            cookies.set('userId', response.headers.userid, { path: '/' });
            cookies.set('accessToken', response.headers.payqrapiauthorization, { path: '/' });
            this.setState({step: "success"});
        }).catch(err=>{
            console.log(err);
            this.setState({errors: ['Вы ввели неверные данные.']});
        })
    }


    render(){
        const errors = this.state.errors.map((e, i) => <li key={i}>{e}</li>);

        switch(this.state.step){
            case "register":
                return(
                    <div>
                        <h1>Вход</h1>
                        <div><label>Номер телефона: <input type="text" name="login" onChange={this.stateChange}/></label></div>
                        <div><label>Пароль: <input type="text" name="password" onChange={this.stateChange}/></label></div>
                        <div><button onClick={this.login}>Продолжить оформление</button></div>
                        <ul className="error">{errors}</ul>
                    </div>
                )
            case "success":
                return(
                    <div>
                        <h1>Пользователь успшено залогинен</h1>
                        <div><a href="/">Перейти в каталог</a></div>
                    </div>
                )
        }
    }
}


export default Login