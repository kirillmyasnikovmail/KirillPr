import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, Link, Redirect, withRouter } from 'react-router-dom'
import Catalog from '../catalog/module'
import Profile from '../profile/module'
import Feedback from '../feedback/module'
import Faq from '../faq/module'
import Action from '../action/action'
import Oferta from '../oferta'
import Register from '../register/module'
import Login from '../login/module'
import Header from '../header/module'
import Home from '../home'
import NotFound from '../not-found/module'
import ReactCustomGA from '../react-custom-ga/module';

class Layout extends PureComponent {
  componentWillMount () {
    this.unlisten = this.props.history.listen(location => {
      // Скролл вверх
      parent.postMessage({top: true}, '*')
      window.scrollTo(0,0)
      
      ReactCustomGA.Change(location.pathname)
    })
  }

  componentWillUnmount () {
    this.unlisten()
  }

  componentDidUpdate () {
    setTimeout(() => {
      parent.postMessage({height: document.getElementById('main').scrollHeight}, '*')
    }, 1000)
  }

  render () {
    let findOffer = null
    if (this.props.catalog.length) {
      const offer = window.location.pathname.split('/')[2]
      findOffer = this.props.catalog.find(item => item._partnerLink.split('/')[2] === offer)
    }
    const fixedHeader = findOffer 
      ? false
      : (window.location.pathname.indexOf('partners') !== -1 ? true : false)

    return (
      <div id='root'>
        <div className='wrapper' id='top'>
          <Header profile={this.props.state.profile}/>
          <main className={`main ${fixedHeader ? 'main-with-fix-header' : ''}`}>
            <div className='container clearfix'>
              <Switch>
                <Route path='/' exact component={Home}/>
                <Route path='/partners' component={Catalog}/>
                <Route path='/profile' component={Profile}/>
                <Route path='/action' component={Action}/>
                <Route path='/feedback' component={Feedback}/>
                <Route path='/oferta' component={Oferta}/>
                <Route path='/faq/:question?' component={Faq}/>
                <Route path='/register' component={Register}/>
                <Route path='/login' component={Login}/>
                <Route component={NotFound}/>
              </Switch>
            </div>
          </main>
        </div>
      </div>
    )
  }
}

export default withRouter(connect(
  state => ({
    catalog: state.catalogState.data
  })
)(Layout))
