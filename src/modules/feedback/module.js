
import React from 'react';
import axios from 'axios';

import * as helpers from "../../helpers";
import Faq from "../faq/module";

class Feedback extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            errors: [],
            name: "",
            email: "",
            title: "",
            comment: "",
            seccuess: false
        }
        this.stateChange = this.stateChange.bind(this);
        this.request = this.request.bind(this);
    }

    request(e){
        const errors = [];
        if(!this.state.name){
            errors.push("name");
        }
        if(!this.state.email){
            errors.push("email");
        }
        else if(!helpers.validateEmail(this.state.email)){
            errors.push("email");
        }
        if(!this.state.title){
            errors.push("title");
        }
        if(!this.state.comment){
            errors.push("comment");
        }
        if(errors.length > 0){
            this.setState({errors: errors});
            return false;
        }

        axios.post("support/request", {
            name: this.state.name,
            email: this.state.email,
            title: this.state.title,
            comment: this.state.comment
        }).then(response=>{
            console.log(response)
            this.refs.name.value = "";
            this.refs.email.value = "";
            this.refs.title.value = "";
            this.refs.comment.value = "";
            this.setState({ errors: [], seccuess: true });

        }).catch(err=>{
            console.log(err);
            this.setState({errors: ["Ошибка сервера. Повторите попытку позже"]});
        })
    }

    render(){
        console.log(this.state.errors, this.state.errors.indexOf("name"));

        return(
            <div>
                <div className="content-top-info feedback">
                    <form className="feedback-form" action="" method="">
                        <div className="feedback-form_row">
                            <input className={"input input-gray feedback-form_client-info " + (this.state.errors.indexOf("name")!=-1 ? "error":"")}
                                   onChange={this.stateChange}
                                   type="text" name="name" ref="name" placeholder="Ваше имя"
                            />
                                <input className={"input input-gray feedback-form_client-info " + (this.state.errors.indexOf("email")!=-1 ? "error":"")}
                                       onChange={this.stateChange}
                                       type="email" name="email" ref="email" placeholder="Электронная почта"
                                />
                        </div>
                        <div className="feedback-form_row">
                            <input className={"input input-gray feedback-form_content " + (this.state.errors.indexOf("title")!=-1 ? "error":"")}
                                    onChange={this.stateChange}
                                    type="text" name="title" ref="title" placeholder="Тема обращения"
                            />
                        </div>
                        <div className="feedback-form_row">
                            <textarea className={"textarea textarea-gray feedback-form_content " + (this.state.errors.indexOf("comment")!=-1 ? "error":"")}
                                  onChange={this.stateChange}
                                  name="comment" ref="comment" placeholder="Расскажите о проблеме"
                            />
                        </div>
	                    {this.state.seccuess ? <p style={{fontSize: '18px', paddingBottom: '25px', fontFamily: 'Roboto', color: 'green'}}>Ваше сообщение отправлено</p> : null}
                        <button className="btn-color-green" onClick={this.request}>Задать вопрос</button>
                    </form>
                </div>
                <Faq/>
           </div>
        )

    }

    stateChange(e){
        const state = {};
        state[e.target.name] = e.target.value;
        this.setState(state)
    }
}




export default Feedback;