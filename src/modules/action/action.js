/**
 * Created by diman on 15.07.17.
 */


import React from 'react';
import s from "./action.css";

class Action extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const extensionLink = `https://chrome.google.com/webstore/detail/workle-%D0%B1%D0%BE%D0%BD%D1%83%D1%81/hpkajobmmfgnmmljdoajmkmohgjcanan?hl=ru`;
        return(
            <div className={s.action}>
                <h3>Подведение итогов акции «Покупай за 0 или Большой Кушбэк на Workle»!</h3>
                <div className={s.info}>
                    <span>Автор: </span>
                    <a target='__blank' href='https://www.workle.ru/profile/1/'>Workle Media</a>
                    <i>11.08.2017 18:09</i>
                </div>
                <div className={s.img}>
                    <img src='https://www.workle.ru/storage/aa/ce/ce/b5/0b/23/5e/98/4c80-2717ce-d189a6.jpg' />
                </div>
                <div className={s.content}>
                    <p>Вот и закончилась акция «Покупай за 0 или Большой Кушбэк на Workle», проходящая с 5 июля по 31 июля 2017 года, и мы с радостью сообщаем вам, что настало время раздавать призы.</p>
                    <p></p>
                    <p>Напоминаем, разыгрывались следующие призы:</p>
                    <ul>
                        <li><p><b>40</b> сертификатов на <b>500 руб.</b></p></li>
                        <li><p><b>15</b> сертификатов на <b>2 000 руб.</b></p></li>
                        <li><p><b>10</b> сертификатов на <b>3 000 руб.</b></p></li>
                        <li><p><b>2</b> сертификата номиналом <b>10 000 руб.</b></p></li>
                    </ul>
                    <p>По итогам эфира <a href='https://www.workle.ru/tv/archive'>WorkleТV</a>, который проходил 10.08.2017г. в 17:00 были определены следующие победители:</p>
                    <p></p>
                    <p>ID победителей, выигравших сертификаты номиналом <b>500 руб.:</b></p>
                    <div className={s.table}>
                        <div className={s.item}>
                            <p>245821</p>
                            <p>106530</p>
                            <p>2273563</p>
                            <p>923037</p>
                            <p>61155</p>
                            <p>257809</p>
                            <p>175299</p>
                            <p>1398564</p>
                            <p>saleinfo@list.ru</p>
                            <p>256397</p>
                            <p>1260982</p>
                            <p>1799018</p>
                            <p>98220</p>
                            <p>1550394</p>
                        </div>
                        <div className={s.item}>
                            <p>191103</p>
                            <p>86873</p>
                            <p>2258513</p>
                            <p>775515</p>
                            <p>152633</p>
                            <p>62159</p>
                            <p>424145</p>
                            <p>2240314</p>
                            <p>376138</p>
                            <p>711606</p>
                            <p>385141</p>
                            <p>2276082</p>
                            <p>112085</p>
                            <p>267025</p>
                        </div>
                        <div className={s.item}>
                            <p>87396</p>
                            <p>1871069</p>
                            <p>947433</p>
                            <p>2206777</p>
                            <p>1901683</p>
                            <p>149071</p>
                            <p>213706</p>
                            <p>30182</p>
                            <p>49715</p>
                            <p>1837831</p>
                            <p>94633</p>
                            <p>2280713</p>
                        </div>
                    </div>
                    <p></p>
                    <p>ID победителей, выигравших сертификаты номиналом <b>2 000 руб.:</b></p>
                    <div className={s.table}>
                        <div className={s.item}>
                            <p>935427</p>
                            <p>245821</p>
                            <p>2273563</p>
                            <p>1260982</p>
                            <p>1837831</p>
                        </div>
                        <div className={s.item}>
                            <p>278230</p>
                            <p>2202346</p>
                            <p>2258513</p>
                            <p>68605</p>
                            <p>30182</p>

                        </div>
                        <div className={s.item}>
                            <p>421797</p>
                            <p>1588699</p>
                            <p>267025</p>
                            <p>872083</p>
                        </div>
                    </div>
                    <p>ID победителей, выигравших сертификаты номиналом <b>3 000 руб.:</b></p>
                    <div className={s.table}>
                        <div className={s.item}>
                            <p>267025</p>
                            <p>1328867</p>
                            <p>1792834</p>
                            <p>68605</p>
                        </div>
                        <div className={s.item}>
                            <p>2276082</p>
                            <p>898328</p>
                            <p>202088</p>
                            <p>94633</p>

                        </div>
                        <div className={s.item}>
                            <p>848921</p>
                            <p>30182</p>
                        </div>
                    </div>
                    <p>ID победителей, выигравших сертификаты номиналом <b>10 000 руб.:</b></p>
                    <div className={s.table}>
                        <div className={s.item}>
                            <p>267025</p>
                        </div>
                        <div className={s.item}>
                            <p>2240314</p>
                        </div>
                        <div className={s.item}>
                        </div>
                    </div>
                    <p></p>
                    <p><strong>Победители, принимайте наши поздравления!</strong></p>
                    <p>Мы выражаем вам благодарность за проявленный интерес и доверие к нашему проекту. Призы будут начислены вам на ваш бонусный счет в течение <strong>7 рабочих дней.</strong></p>
                    <p></p>
                    <p>Желаем Вам дальнейших успехов вместе с Workle! Следите за нашими новостями – впереди много интересных <strong>конкурсов и акций!</strong></p>
                </div>
            </div>
        )
    }
}


export default Action;