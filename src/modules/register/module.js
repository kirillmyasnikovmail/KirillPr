'use strict';

import React from "react";
import axios from 'axios';
import styles from "./module.css"


class Register extends React.Component{

    validateEmail(email){
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }


    constructor(props){
        super(props);
        this.state = {
            errors: [],
            mobilePhone: "",
            email: "",
            confirmationCode: "",
            password: "",
            step: 1,
            steps: 3
        }
    }


    stateChange(e){
        const state = {};
        state[e.target.name] = e.target.value;
        this.setState(state)
    }


    registerPhone(){
        const errors = [];
        if(!this.state.email){
            errors.push("Введите e-mail");
        }
        else {
            if(!this.validateEmail(this.state.email)){
                errors.push("Введите корректный e-mail адрес");
            }
        }
        if(!this.state.mobilePhone){
            errors.push("Введите номер телефона");
        }
        if(errors.length > 0){
            this.setState({errors: errors});
            return false;
        }

        axios.post("users/register", {
            mobilePhone: this.state.mobilePhone,
            email: this.state.email
        }).then(response=>{
            console.log(response)
            this.setState({step: 2})
        }).catch(error=>{
            console.log(error.response);
            this.setState({errors: ['Ошибка сервера. Попробуйте ещё раз.']});
        });
    }

    confirmPhone(){
        const errors = [];
        if(!this.state.password){
            errors.push("Введите пароль");
        }
        else{
            if(this.state.password.length < 4){
                errors.push("Длина пароля должна быть более 4-х символов");
            }
        }
        if(!this.state.confirmationCode){
            errors.push("Введите код подтверждения");
        }
        if(errors.length > 0){
            this.setState({errors: errors});
            return false;
        }

        axios.post("users/profile/mobilephone/confirm", {
            mobilePhone: this.state.mobilePhone,
            password: this.state.password,
            confirmationCode: this.state.confirmationCode
        }).then(response=>{
            console.log(response)
            this.setState({step: 3})
        }).catch(error=>{
            console.log(error);
            this.setState({errors: ['Вы ввели неправильный проверочный код']});
        });
    }





    render(){
        const errors = this.state.errors.map((e, i) => <li key={i}>{e}</li>);

        switch(this.state.step){
            case 1:
                return(
                    <div>
                        <h1>Регистрация</h1>
                        <h4>Шаг {this.state.step} из {this.state.steps}</h4>
                        <div><label>Номер телефона: <input type="text" name="mobilePhone" onChange={this.stateChange.bind(this)}/></label></div>
                        <div><label>E-mail: <input type="text" name="email" onChange={this.stateChange.bind(this)}/></label></div>
                        <div><button onClick={this.registerPhone.bind(this)}>Продолжить оформление</button></div>
                        <ul className="error">{errors}</ul>
                    </div>
                )
            case 2:
                return(
                    <div>
                        <h1>Регистрация</h1>
                        <h4>Шаг {this.state.step} из {this.state.steps}</h4>
                        <div><label>Введите код из смс отправленный на номер {'"' + this.state.mobilePhone + '"'}</label></div>
                        <div><label>Смс код: <input type="text" name="confirmationCode" onChange={this.stateChange.bind(this)}/></label></div>
                        <div><label>Пароль: <input type="password" name="password" onChange={this.stateChange.bind(this)}/></label></div>
                        <div><button onClick={this.confirmPhone.bind(this)}>Продолжить оформление</button></div>
                        <ul className="error">{errors}</ul>
                    </div>
                )
            case 3:
                return(
                    <div>
                        <h1>Регистрация</h1>
                        <h4>Шаг {this.state.step} из {this.state.steps}</h4>
                        <div>Регистрация завершена</div>
                        <div><a href="/">Перейти в каталог</a></div>
                    </div>
                )
        }
    }
}


export default Register

