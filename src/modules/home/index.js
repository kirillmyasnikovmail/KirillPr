import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Home extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            step: false,
            stepImage: null
        }
    }
    render () {
        if (!this.props.catalog.length) {
            return <div>Зарузка...</div>
        }
        const popular = this.props.catalog.sort((curr, next) => next.rating - curr.rating)
        const items = popular.filter((item, index) => index <= 11).map((item, index) => {
            let sticker = index + 1
            return (
                <Link to={item._partnerLink} key={index} className={"main-offers__item main-offers__item--" + sticker}>
                    <div className="main-offers__logo">
                        <img src={'https://payqr.ru' + item.brand.imageUrl}/>
                    </div>
                    {item.promoActions.length > 1
                        ? <div className="main-offers__bonus">Бонусов до <span className="main-offer__amount">{item.promoActions[1].amount.value}{item.promoActions[1].amount.type}</span></div>
                        : <div className="main-offers__bonus">Бонусов до</div>
                    }
                    <div className="main-offers__numb">
                        {item.promoActions[0].amount.value}{item.promoActions[0].amount.type === '%' ? '%' : ''}
                    </div>
                </Link>
            )
        })
        return (
            <div className="wrapper clearfix" onClick={() => {this.setState({step: null}); setTimeout(() => this.setState({stepImage: null}), 450)}}>
                <section className="main-about">
                    <h1 className="main-about__title">Что такое Бонусный клуб Workle?</h1>
                    <p className="main-about__txt">Бонусный клуб Workle - это программа лояльности специально для пользователей проекта Workle. Участники могут получать привелегии при покупках более чем 500 магазинах-партнерах. За каждую покупку участник получает бонусы (1 бонус = 1 рублю). <br/>Полученные бонусы вы просто выводите на баланс телефон, банковскую карту или на банковский счет..</p>
                </section>
                <section className="main-promo">
                    <div className={`main-promo__modal ${this.state.step ? 'active' : ''}`}>
                        <img src={`/images/actions/step-${this.state.stepImage}.png`}/>
                    </div>
                    <div className="main-row">
                        <div className="main-row__step">
                            <img src="/images/steps-1.png" onClick={(e) => {this.setState({step: 1, stepImage: 1}); e.stopPropagation()}}/>
                        </div>
                        <div className="main-row__step main-row__step--txt-up">Перейдите на сайт <br/>магазина из карточки партнера</div>
                        <div className="main-row__step main-row__step--3">
                            <img src="/images/steps-3.png" onClick={(e) => {this.setState({step: 3, stepImage: 3}); e.stopPropagation()}}/>
                        </div>
                        <div className="main-row__step main-row__step--txt-up">Получите бонусы до 30% <br/>на вашу карту, телефон, банковский счет</div>
                    </div>
                    <div className="main-row main-row--round">
                    <div className="step-number">
                        <span className="step-round">1</span>
                        <img className="main-row__stripe" src="/images/line-dotted2.svg"/>
                    </div>
                    <div className="step-number">
                        <span className="step-round">2</span>
                        <img className="main-row__stripe" src="/images/line-dotted2.svg"/>
                    </div>
                    <div className="step-number">
                        <span className="step-round">3</span>
                        <img className="main-row__stripe" src="/images/line-dotted2.svg"/>
                    </div>
                    <div className="step-number">
                        <span className="step-round">4</span>
                    </div>
                    </div>
                    <div className="main-row">
                        <div className="main-row__step main-row__step--txt-down">Выберите любой <br/>магазин в каталоге</div>
                        <div className="main-row__step">
                            <img src="/images/steps-2.png" onClick={(e) => {this.setState({step: 2, stepImage: 2}); e.stopPropagation()}}/>
                        </div>
                        <div className="main-row__step main-row__step--txt-down">Совершите покупку, <br/>как обычно</div>
                        <div className="main-row__step">
                            <img src="/images/steps-4.png" onClick={(e) => {this.setState({step: 4, stepImage: 4}); e.stopPropagation()}}/>
                        </div>
                    </div>
                </section>
                <section className="main-offers">
                    <h2 className="main-offers__title">Покупайте дешевле в 500+ магазинах</h2>
                    <div className="main-offers__list">
                        {items}                         
                    </div>
                </section>
                <a className="banner-sec" href="https://chrome.google.com/webstore/detail/workle-%D0%B1%D0%BE%D0%BD%D1%83%D1%81/hpkajobmmfgnmmljdoajmkmohgjcanan?hl=ru" target="_blank"></a>
                <section className="people-sec">
                    <h2 className="main-offers__title">Нас уже</h2>
                    <div className="people-sec-wrapper">
                    <div className="people-promo-left">
                    </div>
                    <div className="people-promo">
                        <div className="people-counter">
                        <span className="people-counter__numb">200 000+</span>
                        </div>
                        <img className="people-promo__img" src="/images/people.png"/>
                    </div>
                    <div className="people-promo-right">
                    </div>
                    </div>
                </section>
                <section className="sec-start">
                    <h2 className="sec-start__title">Начните совершать покупки уже сейчас</h2>
                    <div className="start-arrow__left">
                    <img src="/images/arrow-left.png"/>
                    </div>
                    <div className="start">
                        <Link className="btn btn--yellow" to='/partners/popular'>Приступить к покупкам</Link>
                    </div>
                    <div className="start-arrow__right">
                    <img src="/images/arrow-right.png"/>
                    </div>
                </section>
                </div>
        )
    }
}

export default connect(
    state => ({
        catalog: state.catalogState.data
    })
)(Home)
