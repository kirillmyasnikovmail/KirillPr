import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { NavLink, withRouter } from 'react-router-dom'
import CatalogHeader from '../catalog/views/header'

class Header extends PureComponent {
  render () {
    const menu = [
      { key: '/', value: 'Главная' },
      { key: '/partners/popular', value: 'Каталог' },
      { key: '/profile', value: 'Личный кабинет'},
      { key: "/partners/favorites", value:  'Избранное' },
      { key: '/action', value: 'Акции' },
      { key: '/oferta', value: 'Оферта' }
    ]
    // Скрываем сортировку на странице офера
    let findOffer = null
    if (this.props.catalog.length) {
      const offer = window.location.pathname.split('/')[2]
      findOffer = this.props.catalog.find(item => item._partnerLink.split('/')[2] === offer)
    }
    const items = menu.map((item, key) => 
      item.value === 'Личный кабинет' && !this.props.profile.id 
        ? null
        : <li key={key}><NavLink exact to={item.key} activeClassName='active'>{item.value}</NavLink></li>
    )
    const fixedHeader = findOffer 
      ? false
      : (window.location.pathname.indexOf('partners') !== -1 ? true : false)

    return (
      <header className={`header ${fixedHeader ? 'header--fix' : ''}`}>
          <div className="mine-menu-blc">
            <div className="container">
              <nav className="mine-menu">
                <ul className="clearfix">
                  {items}
                </ul>
              </nav>
            </div>
          </div>
          {fixedHeader ? <CatalogHeader/> : null}
          {window.location.pathname === '/' ? <LandingHeader/> : null}
        </header>
    )
  }
}

const LandingHeader = () => {
  return (
    <div className="header--main">
      <div className="header-text">
        <div className="header-title">
            <h2 className="header-title__title">ЭКОНОМЬТЕ ДО</h2>
            <h2 className="header-title__subtitle">c каждой покупкой<br/> в интернете</h2>
        </div>
        <div className="header-amount">
            <span className="header-amount__txt">30</span>
        </div>
        <div className="header-value">
            <span className="header-amount__value">%</span>
        </div>
      </div>
    </div>
  )
}

export default withRouter(connect(
  state => ({
    catalog: state.catalogState.data,
    profile: state.profileState.data
  })
)(Header))
