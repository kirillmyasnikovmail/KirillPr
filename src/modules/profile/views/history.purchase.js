'use strict';

import React from "react";
import { Link } from 'react-router-dom';

import HistoryPurchaseTableView from "./history.purchase.table"

const HistoryPurchaseView = (props) => {
    return (
        <div className="lk-bar">
            <a href="#" className="btn btn--back" onClick={()=>window.history.back()}>НАЗАД</a>
            <h1 className="section-title">История покупок</h1>
            <div className="frame-bonus">
                <div className="frame-buy">
                    <HistoryPurchaseTableView
                        operations={props.operations} 
                        cssClassForStatus={props.cssClassForStatus} 
                        cssClassForStatusIcon={props.cssClassForStatusIcon}
                        showElements={99999}
                    />
                </div>
            </div>
        </div>
    );
}

export default HistoryPurchaseView;