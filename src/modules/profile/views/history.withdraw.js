'use strict';

import React from "react";
import { Link } from 'react-router-dom';

import HistoryWithdrawTableView from "./history.withdraw.table"

const HistoryWithdrawView = (props) => {
    return (
        <div className="lk-bar">
            <a href="#" className="btn btn--back" onClick={()=>window.history.back()}>НАЗАД</a>
            <h1 className="section-title">История вывода средств</h1>
            <div className="frame-bonus">
                <div className="frame-buy">
                    <HistoryWithdrawTableView
                        operations={props.operations} 
                        cssClassForStatus={props.cssClassForStatus} 
                        cssClassForStatusIcon={props.cssClassForStatusIcon}
                        showElements={99999}
                    />
                </div>
            </div>
        </div>
    );
}

export default HistoryWithdrawView;