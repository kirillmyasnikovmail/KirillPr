import React, { PureComponent } from "react"
import { Link } from 'react-router-dom'

import HistoryPurchaseTableView from "./history.purchase.table"
import HistoryWithdrawTableView from "./history.withdraw.table"

class ProfileView extends PureComponent {
    render () {
        const profile = this.props.profile,
            withdrawOperations = this.props.withdrawOperations,
            purchaseOperations = this.props.purchaseOperations;

        return (
            <div className="lk-bar">


	            {/**/}
	            <div className="">
		            <div className="content-top-info balance">
			            <div className="balance-status">
				            <div className="balance-item"><span className="balance-item_status balance-item_status-available">Доступно</span>
					            <br/><span className="balance-item_price">{profile.fundsWallets ? profile.fundsWallets[0].amount : "0"}</span>
				            </div>
				            <div className="balance-item"><span className="balance-item_status balance-item_status-expected">Ожидаются</span>
					            <br/><span className="balance-item_price balance-item_price-status-expected">{profile.fundsWallets ? profile.fundsWallets[0].waitingAmount : "0"}</span>
				            </div>
			            </div>
			            <div className="balance-info">
				            <div className="balance-info-item balance-info_min-price">Минимальная сумма для вывода —&#8202; 300</div>
				            <div className="balance-info-item balance-info_conversion">При выводе бонусов конвертация происходит по курсу 1 бонус = 1 рубль</div>
			            </div>
			            <div className="balance-button">
				            <button className="btn-color-green">Вывести бонусы</button>
				            <button className="btn-color-gray"><Link className="menu__link" to="/profile/feedback">Сообщить о проблеме</Link></button>
			            </div>
		            </div>
		            <div className="cacheback-info">
			            <div className="cacheback-info_steps cacheback-info_steps-first">Сумма кэшбэка и информация о покупке появятся в Личном кабинете в течение 2 часов после покупки.</div>
			            <div className="cacheback-info_steps cacheback-info_steps-second">Обычно магазин подтверждает заказ до 4 недель. Кэшбэк будет зачислен на ваш кошелек в следующем за подтверждением месяце.</div>
			            <div className="cacheback-info_steps cacheback-info_steps-third">Информация о дате подтверждения заказа будет доступна в истории операции.</div>
		            </div>
	            </div>
	            {/**/}
	            
                <div className="infographyc">
        			<div className="icn icn--money">Сумма бонусов и информация <br/>о покупке появятся в Личном кабинете <br/>в течение 2 часов после покупки.</div>
        			<div className="icn icn--sandclock">Обычно магазин подтверждает заказ до 4 недель. <br/>Бонусы будут зачислены на ваш кошелек <br/>в следующем за подтверждением месяце.</div>
        			<div className="icn icn--i">Информация о дате подтверждения заказа <br/>будет доступна в истории операции.</div>
                </div>
                <section className="lk-sec">
					<h2 className="section-title">История операций</h2>
                    <HistoryPurchaseTableView
                        operations={purchaseOperations}
                        cssClassForStatus={this.props.cssClassForStatus}
                        cssClassForStatusIcon={this.props.cssClassForStatusIcon}
                    />
                </section>
                <section className="lk-sec">
                    <h2 className="section-title">История вывода средств</h2>
                    <HistoryWithdrawTableView
                        operations={withdrawOperations}
                        cssClassForStatus={this.props.cssClassForStatus}
                        cssClassForStatusIcon={this.props.cssClassForStatusIcon}
                    />
				</section>
            </div>
        )
    }
}

export default ProfileView;