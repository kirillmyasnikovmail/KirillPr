import React, { PureComponent } from "react"
import { Switch, Route, Link, withRouter } from 'react-router-dom'

class ProfileWrap extends  PureComponent {
	constructor(props){
		super(props);
		this.state = {
			activeMenu: 'profile',
		}
	}

	componentWillReceiveProps (nextProps) {
		if (nextProps.location !== this.props.location) {
			let location = nextProps.location.pathname.split('/');
			location = location[location.length -1];
			switch (location) {
				case "profile":
					this.setState({activeMenu: 'profile'});
					break;
				case "favorites":
					this.setState({activeMenu: 'favorites'});
					break;
				case "feedback":
					this.setState({activeMenu: 'feedback'});

			}
		}
	}


	render(){
		const {activeMenu} = this.state;

		return (
			<div className="container-content">
				<div classNameName="sidebar">
					<h2>Бонусный счет</h2>
					<nav className="menu-wrapper">
						<ul className="menu  main__menu" id="menu">
							<li className='menu__item'
							    onClick={() => {this.setState({activeMenu: 'profile'})}}
							>
								<Link className={`menu__link ${activeMenu === 'profile' ? 'active' : '' }`} to="/profile">Мой профиль</Link>
							</li>
							<li className='menu__item'
							    onClick={() => {this.setState({activeMenu: 'favorites'})}}
							>
								<Link className={`menu__link ${activeMenu === 'favorites' ? 'active' : '' }`} to="/profile/favorites">Избранное (5)</Link>
							</li>
							<li className='menu__item'
							    onClick={() => {this.setState({activeMenu: 'feedback'})}}
							>
								<Link className={`menu__link ${activeMenu === 'feedback' ? 'active' : '' }`} to="/profile/feedback">Сообщить о проблеме</Link>
							</li>
							<li className="menu__item"
							>
								<a className="menu__link" href="#">Выход</a>
							</li>
						</ul>
					</nav>
				</div>
				<div className="content profile-design">
					{this.props.children}
				</div>
			</div>
		)
	}
}

export default withRouter(ProfileWrap)