import React, { PureComponent } from "react"
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as helpers from "../../../helpers"
import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru')

class HistoryPurchaseTableView extends PureComponent {
    constructor (props) {
        super(props)
        this.state = {
            showElements: this.props.showElements,
            sort: 'down'
        }
    }

    changeSort (type) {

    }

    render () {
        const props = this.props
        const operations = props.operations
        const cssClassForStatus = props.cssClassForStatus
        const cssClassForStatusIcon = props.cssClassForStatusIcon
        let sort = operations

        if (this.state.sort === 'down') {
            sort = operations.sort((item, prev) => item.date > prev.date ? -1 : 1)
        } else {
            sort = operations.sort((item, prev) => item.date > prev.date ? 1 : -1)
        }

        const purchaseOperations = sort.slice(0, this.state.showElements)
        const items = purchaseOperations.map((item, index) => {
            const findOffer = props.catalog.filter(shop => shop.name === item.shopName)
            if (!findOffer.length) {
                return null
            }
            return (
                <div>
                    <div className="operations-history_row">
                        <div className="operations-history_cell operations-history_img-info">
                            <div className="operations-history_cell_img">
                                <img className="operations-history_img" src="images/megafon.png" />
                            </div>
                            <div className="operations-history_cell_info">
                                <span className="operations-history_name_brand">Покупка в&ensp;
                                    <Link to={findOffer[0]._partnerLink} className="flex__img">
                                        {item.shopName}
                                    </Link>
                                </span>
                                <div className="operations-history_cell_price">
                                    <span>Номер покупки:&ensp;{item.number}</span>
                                    <br /><span>Сумма покупки:&ensp; {item.customData.orderAmount} {item.customData.orderCurrency == 'USD' ? '$' : '₽'}</span>
                                </div>
                            </div>
                        </div>
                        <div className="operations-history_cell">
	                        {moment(item.date).format('LLL')}
                        </div>
                        <div className="operations-history_cell">
                            <span className={`operations-history_status operations-history_status-confirmed ${cssClassForStatusIcon[item.status]}`}>
                                {item.status === 'Cancelled' ? 'Отменено' : item.status === 'Charging' ? 'Ожидается подтверждение' : 'Подтверждено'}
                            </span>
                            <br /><span className="operations-history_awaiting-confirmation" />
                        </div>
                        <div className="operations-history_cell">
                            <span className={`operations-history_bonus operations-history_bonus-confirmed ${cssClassForStatus[item.status]}`}>
                                {item.status === 'Cancelled' ? '--' : item.status === 'Charging' ? ('+' + item.amount) : ('+' + item.amount)}
                            </span>
                        </div>
                    </div>
                    {/**/}
                    {/**/}
                    {/**/}
                   {/* <div key={index} className="flex__row">
                    <div className="flex__col flex__col--status">
                    <div className="flex__cont flex__cont-name d-hid">Статус</div>
                    <div className={`flex__cont flex__status ${cssClassForStatusIcon[item.status]}`}>{item.status == 'Cancelled' ? 'Отменено' : item.status == 'Charging' ? 'Ожидается подтверждение' : 'Подтверждено'}</div>
                    </div>
                    <div className="flex__col flex__col--bonus">
                    <div className="flex__cont flex__cont-name d-hid">Бонусы</div>
                    <div className="flex__cont">
                    <span className={`flex__bonus-count ${cssClassForStatus[item.status]}`}>{item.status == 'Cancelled' ? '--' : item.status == 'Charging' ? ('+' + item.amount) : ('+' + item.amount)}</span>
                    </div>
                    </div>
                    </div>*/}
                </div>
            )
        })

        return (
            <div>
                <div classNameName="operations-history">
                    <div className="operations-history_header-table">
                        <div className="operations-history_cell">Информация о покупке</div>
                        <div className="operations-history_cell" onClick={() => this.setState({sort: this.state.sort === 'up' ? 'down' : 'up'})}
                             style={{cursor: 'pointer'}}
                        >
                            Дата операции
                            <span style={{padding: '0px 10px 0px 15px'}} className={`btn flex__filter icon-up-24 ${this.state.sort === 'up' ? 'active' : ''}`} />
                            <span style={{padding: '0px 10px 0px 15px'}} className={`btn flex__filter icon-down-24 ${this.state.sort === 'down' ? 'active' : ''}`} />
                        </div>
                        <div className="operations-history_cell">Статус</div>
                        <div className="operations-history_cell">Бонусы</div>
                    </div>
	                {!operations.length
		                ? <div className="flex__emty">Операций пока нет</div>
		                : items
	                }
	                {operations.length > 3 && this.state.showElements < operations.length ? <Link to='/profile/history/purchase' onClick={() => {
		                setTimeout(() => {
			                parent.postMessage({height: document.getElementById('main').scrollHeight}, '*')
		                }, 1000)
	                }} className="btn btn--more">Показать еще</Link> : null}
                </div>

            </div>
        )
    }
}

HistoryPurchaseTableView.defaultProps = {
    showElements: 3
}

export default connect(
    state => ({
        catalog: state.catalogState.data
    })
)(HistoryPurchaseTableView)
