import React, { PureComponent } from "react";
import { Link } from 'react-router-dom'
import * as helpers from "../../../helpers";
import moment from 'moment';
import 'moment/locale/ru';

moment.locale('ru')

class HistoryWithdrawTableView extends PureComponent {
    constructor (props) {
        super(props)
        this.state = {
            showElements: this.props.showElements,
            sort: 'down'
        }
    }
    render () {
        const operations = this.props.operations,
        cssClassForStatus = this.props.cssClassForStatus,
        cssClassForStatusIcon = this.props.cssClassForStatusIcon;
        let sort = operations

        if (this.state.sort === 'down') {
            sort = operations.sort((item, prev) => item.date > prev.date ? -1 : 1)
        } else {
            sort = operations.sort((item, prev) => item.date > prev.date ? 1 : -1)
        }
        
        const withdrawOperations = operations.slice(0, this.state.showElements)
        const items = withdrawOperations.map((item, index) =>
            <div key={index} className="flex__row">
                <div className="flex__col flex__col--main">
                    <div className="flex__cont flex__cont-name d-hid">Информация о покупке</div>
                    <div className="flex__out-main flex__cont">
                        <div className={`flex__out-main-numb  ${item.customData.bankCardNumber ? 'flex__out-main-numb--card' : (item.customData.mobilePhone ? 'flex__out-main-numb--tel' : 'flex__out-main-numb--card')}`}>
                            {item.customData.bankCardNumber
                                ? item.customData.bankCardNumber.substr(0, 4) + ' •••• •••• ' + item.customData.bankCardNumber.substr(12, 4)
                                : item.customData.mobilePhone !== undefined ? ` +7 (${item.customData.mobilePhone.substr(1, 3)}) ${item.customData.mobilePhone.substr(4, 3)}-${item.customData.mobilePhone.substr(7, 2)}-${item.customData.mobilePhone.substr(9, 2)}` : ''
                            }
                        </div>
                        <div className="flex__datas">Номер заявки: {item.number}</div>
                    </div>
                </div>
                <div className="flex__col flex__col--date">
                    <div className="flex__cont flex__cont-name d-hid">Дата операции</div>
                    <div className="flex__cont">
                        <div className="flex__dat">{moment(item.date).format('LLL')}</div>
                    </div>
                </div>
                <div className="flex__col flex__col--status">
                    <div className="flex__cont flex__cont-name d-hid">Статус</div>
                    <div className={`flex__cont flex__status ${cssClassForStatusIcon[item.status]}`}>{item.status == 'Reverted' ? 'Отменено' : item.status == 'Charging' ? 'Ожидается подтверждение' : 'Бонусы выведены'}</div>
                </div>
                <div className="flex__col flex__col--bonus">
                    <div className="flex__cont flex__cont-name d-hid">Бонусы</div>
                    <div className="flex__cont">
                        <span className={`flex__bonus-count ${cssClassForStatus[item.status]}`}>{item.status == 'Reverted' ? '--' : item.status == 'Reverted' ? (item.amount) : ('-' + item.amount)}</span>
                    </div>
                </div>
            </div>
        )
        return (
            <div className="blc-table output-table">

                <div classNameName="operations-history">
                    <div className="operations-history_header-table">
                        <div className="operations-history_cell">Информация о покупке</div>
                        <div className="operations-history_cell" onClick={() => this.setState({sort: this.state.sort === 'up' ? 'down' : 'up'})}
                             style={{cursor: 'pointer'}}
                        >
                            Дата операции
                            <span style={{padding: '0px 10px 0px 15px'}} className={`btn flex__filter icon-up-24 ${this.state.sort === 'up' ? 'active' : ''}`} />
                            <span style={{padding: '0px 10px 0px 15px'}} className={`btn flex__filter icon-down-24 ${this.state.sort === 'down' ? 'active' : ''}`} />
                        </div>
                        <div className="operations-history_cell">Статус</div>
                        <div className="operations-history_cell">Бонусы</div>
                    </div>
		            {!operations.length
			            ? <div className="flex__emty">Вывода средств пока нет</div>
			            : items
		            }
		            {operations.length > 3 && this.state.showElements < operations.length ? <Link to='/profile/history/purchase' onClick={() => {
			            setTimeout(() => {
				            parent.postMessage({height: document.getElementById('main').scrollHeight}, '*')
			            }, 1000)
		            }} className="btn btn--more">Показать еще</Link> : null}
                </div>
            </div>
        )
    }
}

HistoryWithdrawTableView.defaultProps = {
    showElements: 3
}

export default HistoryWithdrawTableView;