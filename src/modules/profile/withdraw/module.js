'use strict';

import React, { PureComponent } from "react";
import { connect } from 'react-redux'
import axios from 'axios';
import { Link } from 'react-router-dom';
import Select  from 'react-select'

import MaskedInput from 'react-maskedinput';

import * as helpers from "../../../helpers";
import styles from "./module.css";
import * as webAPI from "../../../api/webapi";

const WITHDRAW_BANKCARD = "WITHDRAW_BANKCARD";
const WITHDRAW_MOBILE = "WITHDRAW_MOBILE";
const WITHDRAW_BANK_ACCOUNT = "WITHDRAW_BANK_ACCOUNT";

const STATUS_START = "STATUS_START";
const STATUS_PROCESS = "STATUS_PROCESS";
const STATUS_SUCCESS = "STATUS_SUCCESS";
const STATUS_ERROR = "STATUS_ERROR";

import * as bankList from '../../../bankList.json'
const bakListItems = bankList.bik.map(item => ({ value: item.name, label: item.name }))

class ProfileWithdraw extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            errors: [],
            mobilePhoneNumber: "",
            mobilePhoneAmount: "",
            mobilePhoneWithdraw: false,
            bankCardPan: "",
            bankCardAmount: "",
            bankCardWithdraw: false,
            bankAccountWithdraw: false,
            withdrawType: WITHDRAW_BANKCARD,
            status: STATUS_START,

            // Данные формы "на банковскую карту"
            bankAccountBank: '',
            bankAccountScore: '',
            bankAccountBIK: '',
            bankAccountNumberScore: '',
            bankAccountUser: '',
            bankAccountAmount: '',

            showBankCardForm: true,
            showMobileForm: true


        }
        this.bankCardStateChange = this.bankCardStateChange.bind(this);
        this.mobilePhoneStateChange = this.mobilePhoneStateChange.bind(this);
        this.withdrawMobilePhone = this.withdrawMobilePhone.bind(this);
        this.withdrawBankCard = this.withdrawBankCard.bind(this);
        this.updateProfile = this.updateProfile.bind(this);
        this.clearBankCardPan = this.clearBankCardPan.bind(this);
    }

    bankCardStateChange(e) {
        let bankCardWithdraw = false,
            bankCardPan = this.state.bankCardPan,
            bankCardAmount = this.state.bankCardAmount;

        switch (e.target.name) {
            case "bankCardPan":
                bankCardPan = this.clearBankCardPan(e.target.value);
                break;
            case "bankCardAmount":
                e.target.value = e.target.value.replace(/\D/g, '');
                if (e.target.value > this.getMaxBankCardAmount()) {
                    e.target.value = this.getMaxBankCardAmount();
                }
                bankCardAmount = e.target.value;
                break;
        }
        if (bankCardAmount >= this.getMinBankCardAmount() && bankCardPan.length == 16) {
            bankCardWithdraw = true;
        }
        this.setState({
            bankCardPan: bankCardPan,
            bankCardAmount: bankCardAmount,
            bankCardWithdraw: bankCardWithdraw,
        });
    }

    bankAccountStateChange (e) {
        let withdraw = false

        // Объем
        const fio = this.state.bankAccountUser.split(" ");
        console.log(fio)
        console.log(fio.length)

        if (this.state.bankAccountAmount >= this.getMinBankAccountAmount() 
            && this.state.bankAccountBank !== ''
            && this.state.bankAccountBIK !== ''
            && this.state.bankAccountScore !== ''
            && this.state.bankAccountNumberScore !== ''
            && this.state.bankAccountUser !== ''
            && fio.length === 3
            && this.state.bankAccountNumberScore.length === 20
            && this.state.bankAccountBIK.length === 9
        ){
            withdraw = true
        } else {
            
        }

        const name = e.target.name
        if (name === 'bankAccountAmount') {
            e.target.value.replace(/\D/g, '')

            if (e.target.value >= this.getMinBankAccountAmount() 
                && this.state.bankAccountBank !== ''
                && this.state.bankAccountBIK !== ''
                && this.state.bankAccountScore !== ''
                && this.state.bankAccountNumberScore !== ''
                && this.state.bankAccountUser !== ''
                && fio.length === 3
                && this.state.bankAccountNumberScore.length === 20
                && this.state.bankAccountBIK.length === 9) {
                    withdraw = true
                }
        }

        // сохраняем данные 
        this.setState({
            [name]: e.target.value,
            bankAccountWithdraw: withdraw
        })
    }

    mobilePhoneStateChange(e) {
        let mobilePhoneWithdraw = false,
            mobilePhoneNumber = this.state.mobilePhoneNumber,
            mobilePhoneAmount = this.state.mobilePhoneAmount;

        switch (e.target.name) {
            case "mobilePhoneNumber":
                mobilePhoneNumber = this.clearBankCardPan(e.target.value);
                break;
            case "mobilePhoneAmount":
                e.target.value = e.target.value.replace(/\D/g, '');
                if (e.target.value > this.getMaxMobilePhoneAmount()) {
                    e.target.value = this.getMaxMobilePhoneAmount();
                }
                mobilePhoneAmount = e.target.value;
                break;
        }
        if (mobilePhoneAmount >= this.getMinMobilePhoneAmount() && mobilePhoneNumber.length == 11) {
            mobilePhoneWithdraw = true;
        }
        this.setState({
            mobilePhoneNumber: mobilePhoneNumber,
            mobilePhoneAmount: mobilePhoneAmount,
            mobilePhoneWithdraw: mobilePhoneWithdraw,
        });
    }

    getMinBankCardAmount() {
        let amount = 0;
        if (this.props.profile.withdrawalTypes) {
            const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "bankcard");
            if (withdraw[0]) {
                amount = withdraw[0].amountFrom;
            }
        }
        return amount;
    }

    getMaxBankCardAmount() {
        let amount = 0;
        if (this.props.profile.withdrawalTypes) {
            const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "bankcard");
            if (withdraw[0]) {
                amount = withdraw[0].amountTo;
            }
        }
        return amount;
    }


    getMinBankAccountAmount() {
        let amount = 0;
        if (this.props.profile.withdrawalTypes) {
        const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "bankaccount");
        if (withdraw[0]) {
            amount = withdraw[0].amountFrom;
        }
        }
        return amount;
    }

    getMaxBankAccountAmount() {
      let amount = 0;
      if (this.props.profile.withdrawalTypes) {
        const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "bankcard");
        if (withdraw[0]) {
          amount = withdraw[0].amountTo;
        }
      }
      return amount;
    }


  getMinMobilePhoneAmount() {
        let amount = 0;
        if (this.props.profile.withdrawalTypes) {
            const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "mobilephone");
            if (withdraw[0]) {
                amount = withdraw[0].amountFrom;
            }
        }
        return amount;
    }

    getMaxMobilePhoneAmount() {
        let amount = 0;
        if (this.props.profile.withdrawalTypes) {
            const withdraw = this.props.profile.withdrawalTypes.filter(i => i.typeName.toLowerCase() == "mobilephone");
            if (withdraw[0]) {
                amount = withdraw[0].amountTo;
            }
        }
        return amount;
    }

    getAmount() {
        return this.props.profile.fundsWallets[0].amount;
    }

    updateProfile() {
        webAPI.getProfile();
        webAPI.getOperations();
    }

    clearBankCardPan(card) {
        return card.replace(/[^0-9.]/g, '');
    }

    changeWithdrawType(i) {
        this.setState({ withdrawType: i });
    }

    withdrawBankCard() {
        if (this.state.bankCardWithdraw) {
            this.setState({ status: STATUS_PROCESS });
            axios.post(`withdrawals/${helpers.guid()}/bankcard`, {
                bankCardPan: this.state.bankCardPan,
                amount: this.state.bankCardAmount,
            }).then(response => {
                console.log(response);
                this.updateProfile();
                this.setState({ status: STATUS_SUCCESS });
            }).catch(error => {
                console.log(error)
                    //check if error is timeout
                if (error.toString().indexOf("timeout") != -1 && error.toString().indexOf("20000ms") != -1) {
                    this.setState({ status: STATUS_SUCCESS });
                } else {
                    this.setState({
                        status: STATUS_ERROR,
                        errors: ["Ошибка сервера. Повторите попытку позже"],
                    });
                }
            })
        }
    }

  withdrawBankAccount() {
    const errors = [];
    
    if (this.state.bankAccountWithdraw) {
      const fio = this.state.bankAccountUser.split(" ");

      // Проверка номера счета
      if(this.state.bankAccountNumberScore.length != 20){
        errors.push("bankAccountNumberScore");
      }

      // Проверка БИК
      if(this.state.bankAccountBIK.length != 9){
        errors.push("bankAccountBIK");
      }
      
      // ФИО
      if(fio.length != 3){
        errors.push("fio");
      }
      console.log(errors)
      // Ошибки
      if(errors.length > 0){
        console.log('Есть ошибки')
        this.setState({errors: errors});
      } else {
        console.log('все отлично')
        this.setState({ status: STATUS_PROCESS });
        axios.post(`withdrawals/${helpers.guid()}/bankaccount`, {
          amount: this.state.bankAccountAmount,
          receiverAccount: helpers.clearExceptDigits(this.state.bankAccountNumberScore),
          receiverBic: helpers.clearExceptDigits(this.state.bankAccountBIK),
          receiverInn: 0,
          lastName: fio[0],
          firstName: fio[1],
          middleName: fio[2]
        }).then(response => {
          console.log(response);
          this.updateProfile();
          this.setState({ status: STATUS_SUCCESS });
        }).catch(error => {
          console.log(error)
          //check if error is timeout
          if (error.toString().indexOf("timeout") != -1 && error.toString().indexOf("20000ms") != -1) {
            this.setState({ status: STATUS_SUCCESS });
          } else {
            this.setState({
              status: STATUS_ERROR,
              errors: ["Ошибка сервера. Повторите попытку позже"],
            });
          }
        })
      }
    }
  }

    withdrawMobilePhone() {
        if (this.state.mobilePhoneWithdraw) {
            this.setState({ status: STATUS_PROCESS });
            axios.post(`withdrawals/${helpers.guid()}/mobile`, {
                mobilePhone: this.state.mobilePhoneNumber,
                amount: this.state.mobilePhoneAmount,
            }).then(response => {
                console.log(response)
                this.updateProfile();
                this.setState({ status: STATUS_SUCCESS });
            }).catch(error => {
                console.log(error)
                    //check if error is timeout
                if (error.toString().indexOf("timeout") != -1 && error.toString().indexOf("20000ms") != -1) {
                    this.setState({ status: STATUS_SUCCESS });
                } else {
                    this.setState({
                        status: STATUS_ERROR,
                        errors: ["Ошибка сервера. Повторите попытку позже"],
                    });
                }
            })
        }
    }

    render(){
        let mobilePhoneList = []
        let bankCardList = []
        let bankAccountList = []
        
        if (this.props.operations.length) {
            mobilePhoneList = this.props.operations.map(item => item.customData.mobilePhone).filter(n => n).slice(0,5);
            bankCardList = this.props.operations.map(item => item.customData.bankCardNumber).filter(n => n).slice(0,5);
            bankAccountList = [...new Set(this.props.operations.map(item => item.customData.bankAccountNumber))].filter(n => n).slice(0,5);
        } 
        
        if(this.props.profile.id){
            const mobilePhoneAmountPlaceHolder = `от ${this.getMinMobilePhoneAmount()} до ${this.getMaxMobilePhoneAmount()}`;
            const bankCardAmountPlaceHolder = `от ${this.getMinBankCardAmount()} до ${this.getMaxBankCardAmount()}`;
            const bankAccountCardAmountPlaceHolder = `от ${this.getMinBankAccountAmount()} до ${this.getMaxBankAccountAmount()}`;

            const amount = this.state.withdrawType === WITHDRAW_BANKCARD
                        ? this.state.bankCardAmount
                        : (this.state.withdrawType === WITHDRAW_MOBILE ? this.state.mobilePhoneAmount : this.state.bankAccountAmount)
            return (
                <section className="lk-bar">        
        			<a href="#" className="btn btn--back" onClick={()=>window.history.back()}>НАЗАД</a>			
        			<h1 className="section-title">Вывод баллов</h1> 
        			<div className="blc output-blc">
        				<div className="output-blc__count">Бонусов доступно: <b>{this.props.profile.fundsWallets[0].amount}</b></div>
        				<div className="output-blc__form">
        					<h3 className="output-blc__form-header">Куда вам удобно вывести?</h3>
        					<div className="output-blc__checkrow clearfix">
        						<button onClick={this.changeWithdrawType.bind(this, WITHDRAW_BANKCARD)} className={`btn output-blc__output-check ${this.state.withdrawType === WITHDRAW_BANKCARD ? 'active' : ''}`}>
        							<span className="output-blc__output-check-img">
        								<img src="/images/icn-card-check-out.svg" alt=""/>
        							</span>
        							<span className="output-blc__output-check-txt">на банковскую карту</span>
        						</button>
        						<button onClick={this.changeWithdrawType.bind(this, WITHDRAW_MOBILE)} className={`btn output-blc__output-check ${this.state.withdrawType === WITHDRAW_MOBILE ? 'active' : ''}`}>
        							<span className="output-blc__output-check-img">
        								<img src="/images/icn-tel-check-out.svg" alt=""/>
        							</span>
        							<span className="output-blc__output-check-txt">на мобильный номер</span>
                                </button>
                                <button onClick={this.changeWithdrawType.bind(this, WITHDRAW_BANK_ACCOUNT)} className={`btn output-blc__output-check ${this.state.withdrawType === WITHDRAW_BANK_ACCOUNT ? 'active' : ''}`}>
                                    <span className="output-blc__output-check-img">
        								<img src="/images/icn-bank-check.svg" alt=""/>
        							</span>
        							<span className="output-blc__output-check-txt">на банковский счёт</span>
        						</button>
        					</div>
        				</div>
                        {this.state.withdrawType === WITHDRAW_BANKCARD
                            ? (
                                <div>
                                    <div className="output-blc__change-items-blc">
                                        <div className="output-blc__change-item output-blc__change-item--card">
                                            {bankCardList.length > 0
                                                ? (
                                                    <div>
                                                        <label className="output-blc__label output-blc__label-short">
                                                            <span className="output-blc__label-name">Сумма бонусов*</span>
                                                            <input className="input output-blc__input-short" type="text" ref="bankCardAmount" name="bankCardAmount" defaultValue={this.state.bankCardAmount} placeholder={bankCardAmountPlaceHolder} onChange={this.bankCardStateChange}/>
                                                        </label>
                                                        {bankCardList.map((item, index) =>
                                                            <div key={index} className="output-blc__change-row">
                                                                <label className="output-blc__change-label">
                                                                    <input type="radio" value={item} name="bankCardPan" defaultChecked={item === this.state.bankCardPan} onClick={(e) => {
                                                                        this.setState({
                                                                            showBankCardForm: false
                                                                        })
                                                                        this.bankCardStateChange(e)
                                                                    }}/>
                                                                    <span className="dop"></span>
                                                                    <span className="output-blc__change-label-name">{item.substr(0, 4)} **** {item.substr(12, 4)}</span>
                                                                </label>
                                                                <button className="btn output-blc__change-del">Удалить</button>
                                                            </div>
                                                        )}
                                                        <div className="output-blc__change-row">
                                                            <label className="output-blc__change-label">
                                                                <input type="radio" name="bankCardPan" value="new" defaultChecked={this.state.showBankCardForm} onClick={(e) => {
                                                                    this.setState({
                                                                        showBankCardForm: true,
                                                                        bankCardPan: ''
                                                                    })
                                                                }}/>
                                                                <span className="dop"></span>
                                                                <span className="output-blc__change-label-name">Добавить новую банковскую карту</span>
                                                            </label>
                                                        </div>
                                                        {this.state.showBankCardForm
                                                            ? (
                                                                <div className="noutput-blc__change-row">
                                                                    <label className="output-blc__label output-blc__label-middle">
                                                                        <MaskedInput className="input" placeholder="Введите номер карты" mask="1111 1111 1111 1111" size="20" ref="bankCardPan" name="bankCardPan" value={this.state.bankCardPan}  onChange={this.bankCardStateChange}/>
                                                                    </label>
                                                                </div>
                                                            )
                                                            : null
                                                        }
                                                    </div>
                                                )
                                                : (
                                                    <div className="output-blc__label-row clearfix">
                                                        <label className="output-blc__label output-blc__label-middle">
                                                            <span className="output-blc__label-name">Банковская карта*</span>
                                                            <MaskedInput className="input" placeholder="Введите номер карты" mask="1111 1111 1111 1111" size="20" ref="bankCardPan" name="bankCardPan" value={this.state.bankCardPan}  onChange={this.bankCardStateChange}/>
                                                        </label>
                                                        <label className="output-blc__label output-blc__label-short">
                                                            <span className="output-blc__label-name">Сумма бонусов*</span>
                                                            <input className="input output-blc__input-short" type="text" ref="bankCardAmount" name="bankCardAmount" defaultValue={this.state.bankCardAmount} placeholder={bankCardAmountPlaceHolder} onChange={this.bankCardStateChange}/>
                                                        </label>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                    <div className="output-blc__change-row">
                                        <label className="output-blc__change-label">
                                            <input type="checkbox" name="save" value="on"/>
                                            <span className="dop"></span>
                                            <span className="output-blc__change-label-name">Сохранить банковскую карту</span>
                                        </label>
                                    </div>
                                    <div className="output-blc__output-btn-bar">
                                        <button className="btn btn--blue output-blc__output-btn" onClick={this.withdrawBankCard} disabled={!this.state.bankCardWithdraw}>Вывести</button>
                                    </div>
                                </div>
                            )
                            : null
                        }
                        {this.state.withdrawType === WITHDRAW_BANK_ACCOUNT
                            ? (
                                <div>
                                    <div className="output-blc__change-items-blc">
                                        <div className="output-blc__change-item output-blc__change-item--card">
                                            <div className="output-blc__label-row clearfix">
                                                <label className="output-blc__label output-blc__label-short">
                                                    <span className="output-blc__label-name">Сумма бонусов*</span>
                                                    <input name="bankAccountAmount" className="input output-blc__input-short" type="number" defaultValue={this.state.bankAccountAmount} placeholder={bankAccountCardAmountPlaceHolder} onChange={(e) => this.bankAccountStateChange(e)}/>
                                                </label>
                                            </div>
                                            <div className="output-blc__label-row clearfix">
                                                <label className="output-blc__label output-blc__label-middle">
                                                    <Select
                                                        value={this.state.bankAccountBank}
                                                        options={bakListItems}
                                                        placeholder='Наименование банка'
                                                        onChange={(value) => {
                                                            const find = bankList.bik.filter(item => item.name === value.value)
                                                            this.setState({
                                                                bankAccountBank: find[0].name,
                                                                bankAccountScore: find[0].ks,
                                                                bankAccountBIK: find[0].bik
                                                            })
                                                        }}
                                                    />
                                                </label>
                                                <label className="output-blc__label output-blc__label-middle">
                                                    <MaskedInput name="bankAccountScore" className="input" placeholder="Корреспондентский счет" mask="11111111111111111111" size="20" value={this.state.bankAccountScore}  onChange={(e) => this.bankAccountStateChange(e)}/>
                                                </label>
                                            </div>
                                            <div className="output-blc__label-row clearfix">
                                                <label className="output-blc__label output-blc__label-middle">
                                                    <MaskedInput name="bankAccountBIK" className="input" placeholder="БИК" mask="111111111" size="9" value={this.state.bankAccountBIK} onChange={(e) => this.bankAccountStateChange(e)}/>
                                                </label>
                                                <label className="output-blc__label output-blc__label-middle">
                                                    <MaskedInput name="bankAccountNumberScore" className="input" placeholder="Номер счета" mask="11111111111111111111" size="20" value={this.state.bankAccountNumberScore} onChange={(e) => this.bankAccountStateChange(e)}/>
                                                </label>
                                            </div>
                                            <div className="output-blc__label-row clearfix">
                                                <label className="output-blc__label output-blc__label-middle">
                                                    <input name="bankAccountUser" className="input" type="text" placeholder={'ФИО (как в паспорте без сокращений)'} defaultValue={this.state.bankAccountUser} onChange={(e) => this.bankAccountStateChange(e)}/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="output-blc__change-row">
                                        <label className="output-blc__change-label">
                                            <input type="checkbox" name="save" value="on"/>
                                            <span className="dop"></span>
                                            <span className="output-blc__change-label-name">Сохранить банковский счет</span>
                                        </label>
                                    </div>
                                    <div className="output-blc__output-btn-bar">
                                        <button className="btn btn--blue output-blc__output-btn" onClick={this.withdrawBankAccount.bind(this)}  disabled={!this.state.bankAccountWithdraw}>Вывести</button>
                                    </div>
                                </div>
                            )
                            : null
                        }
                        {this.state.withdrawType === WITHDRAW_MOBILE
                            ? (
                                <div>
                                    <div className="output-blc__change-items-blc">
                                        <div className="output-blc__change-item output-blc__change-item--card">
                                            {mobilePhoneList.length > 0
                                                ? (
                                                    <div>
                                                        <label className="output-blc__label output-blc__label-short">
                                                            <span className="output-blc__label-name">Сумма бонусов*</span>
                                                            <input className="input output-blc__input-short" type="text" ref="mobilePhoneAmount" name="mobilePhoneAmount" defaultValue={this.state.mobilePhoneAmount} placeholder={mobilePhoneAmountPlaceHolder} onChange={this.mobilePhoneStateChange}/>
                                                        </label>
                                                        {mobilePhoneList.map((item, index) =>
                                                            <div key={index} className="output-blc__change-row">
                                                                <label className="output-blc__change-label">
                                                                    <input type="radio" name="mobilePhoneNumber" value={item} defaultChecked={item === this.state.mobilePhoneNumber} onClick={(e) => {
                                                                        this.setState({
                                                                            showMobileForm: false
                                                                        })
                                                                        this.mobilePhoneStateChange(e)
                                                                    }}/>
                                                                    <span className="dop"></span>
                                                                    <span className="output-blc__change-label-name">{item !== undefined ? ` +7 (${item.substr(1, 3)}) ${item.substr(4, 3)}-${item.substr(7, 2)}-${item.substr(9, 2)}` : ''}</span>
                                                                </label>
                                                                <button className="btn output-blc__change-del">Удалить</button>
                                                            </div>
                                                        )}
                                                        <div className="output-blc__change-row">
                                                            <label className="output-blc__change-label">
                                                                <input type="radio" name="mobilePhoneNumber" value="new" defaultChecked={this.state.showMobileForm} onClick={(e) => {
                                                                    this.setState({
                                                                        showMobileForm: true,
                                                                        mobilePhoneNumber: ''
                                                                    })
                                                                }}/>
                                                                <span className="dop"></span>
                                                                <span className="output-blc__change-label-name">Добавить новый номер телефона</span>
                                                            </label>
                                                        </div>
                                                        {this.state.showMobileForm
                                                            ? (
                                                                <div className="noutput-blc__change-row">
                                                                    <label className="output-blc__label output-blc__label-middle">
                                                                        <MaskedInput className="input" mask="+7 (111) 111-11-11" size="20" ref="mobilePhoneNumber" value={this.state.mobilePhoneNumber} name="mobilePhoneNumber" onChange={this.mobilePhoneStateChange}/>
                                                                    </label>
                                                                </div>
                                                            )
                                                            : null
                                                        }
                                                    </div>
                                                )
                                                : (
                                                    <div className="output-blc__label-row clearfix">
                                                        <label className="output-blc__label output-blc__label-middle">
                                                            <span className="output-blc__label-name">Номер телефона</span>
                                                            <MaskedInput className="input" mask="+7 (111) 111-11-11" size="20" ref="mobilePhoneNumber" value={this.state.mobilePhoneNumber} name="mobilePhoneNumber"  onChange={this.mobilePhoneStateChange}/>
                                                        </label>
                                                        <label className="output-blc__label output-blc__label-short">
                                                            <span className="output-blc__label-name">Сумма бонусов*</span>
                                                            <input className="input output-blc__input-short" type="text" ref="mobilePhoneAmount" name="mobilePhoneAmount" defaultValue={this.state.mobilePhoneAmount} placeholder={mobilePhoneAmountPlaceHolder} onChange={this.mobilePhoneStateChange}/>
                                                        </label>
                                                    </div>
                                                )
                                            }
                                        </div>
                                    </div>
                                    <div className="output-blc__change-row">
                                        <label className="output-blc__change-label">
                                            <input type="checkbox" name="save" value="on"/>
                                            <span className="dop"></span>
                                            <span className="output-blc__change-label-name">Сохранить номер телефона</span>
                                        </label>
                                    </div>
                                    <div className="output-blc__output-btn-bar">
                                        <button className="btn btn--blue output-blc__output-btn" onClick={this.withdrawMobilePhone} disabled={!this.state.mobilePhoneWithdraw}>Вывести</button>
                                    </div>
                                </div>
                            )
                            : null
                        }
                    </div>
                    {this.state.status === STATUS_SUCCESS
                        ? (
                            <div className="pop-up">
                                <div className="pop-up__overlay">
                                    <div className="pop-up__main">
                                        <button className="btn pop-up__close" title="Закрыть"></button>
                                        <div className="pop-up__alert">
                                            <img src="/images/icn-pape-planer.svg" alt=""/>
                                            Заявка на вывод {amount} бонусов <br/>успешно создана
                                        </div>
                                        <div className="pop-up__message">
                                            <p>Совсем скоро бонусы будут зачислены на 
                                                {this.state.withdrawType === WITHDRAW_BANKCARD 
                                                    ? "карту •••• "+this.clearBankCardPan(this.state.bankCardPan).substr(12,4) 
                                                    : (this.state.withdrawType === WITHDRAW_MOBILE
                                                        ? "номер " + this.state.mobilePhoneNumber
                                                        : "банковский счёт " + this.state.bankAccountNumberScore
                                                    )}
                                            </p>
                                            <p>В редких случаях срок зачисления может составлять <br/>
                                            до 3-х рабочих дней.</p>
                                        </div>
                                        <Link to='/partners/popular' className="btn btn--blue pop-up__btn">Продолжить покупки</Link>
                                    </div>
                                </div>
                            </div>
                        )
                        : null
                    }
                    {this.state.status === STATUS_ERROR
                        ? (
                            <div className="pop-up">
                                <div className="pop-up__overlay">
                                    <div className="pop-up__main">
                                        <button className="btn pop-up__close" title="Закрыть" onClick={() => this.setState({status: STATUS_START})}></button>
                                        <div className="pop-up__alert">
                                            При попытке вывода {amount} бонусов <br/>произошла ошибка
                                        </div>
                                        <div className="pop-up__message">
                                            <p>Мы в кратчайшие сроки это исправим. <br />Просим прощения за неудобства</p>
                                        </div>
                                        <Link to='/partners/popular' className="btn btn--blue pop-up__btn">Продолжить покупки</Link>
                                    </div>
                                </div>
                            </div>
                        )
                        : null
                    }
        		</section>
            )
        } else{
            return null
        }
    }
}

export default connect(
    state => ({
        operations: state.operationsState.data
    })
)(ProfileWithdraw)
