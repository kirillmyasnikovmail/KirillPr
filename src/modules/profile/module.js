import React, { PureComponent } from "react"
import { connect } from 'react-redux'
import { Switch, Route, Link, Redirect } from 'react-router-dom'
import * as webAPI from "../../api/webapi";

import ProfileWithdraw from "./withdraw/module"
import ProfileView from "./views/profile"
import HistoryWithdrawView from "./views/history.withdraw"
import HistoryPurchaseView from "./views/history.purchase"
import ProfileWrap from "./views/profile.wrap"
import Feedback from '../feedback/module'
import ProfileFavorites from '../profile/views/favorites'


class Profile extends  PureComponent {
    constructor(props){
        super(props);
    }
    
    componentWillMount () {
        // Скролл вверх
        parent.postMessage({top: true}, '*')
    }

    componentDidMount(){
        webAPI.getProfile();
        webAPI.getOperations();
    }

    render(){
        const withdrawOperations = [],
            purchaseOperations = []

        if (this.props.operations.length > 0) {
            this.props.operations.map(item =>
                item.typeName === "Withdraw"
                    ? withdrawOperations.push(item)
                    : (item.typeName === "AdmitadCashback" 
                        ? purchaseOperations.push(item)
                        : null
                    )
            )
        }
        const cssClassForStatus = {
            "Charged": "flex__bonus-count--green",
            "Paid": "flex__bonus-count--green",
            "Charging": "",
            "Cancelled": "flex__bonus-count--red",
            "Reverted": "flex__bonus-count--red",
        }
        const cssClassForStatusIcon = {
            "Charged": "flex__green",
            "Paid": "flex__green",
            "Charging": "flex__yello",
            "Cancelled": "flex__red",
            "Reverted": "flex__red",
        }
        return (
            <ProfileWrap>
	            <Switch>
	                <Route exact path='/profile' render={() =>
	                    <ProfileView
	                        withdrawOperations={withdrawOperations}
	                        purchaseOperations={purchaseOperations}
	                        profile={this.props.profile}
	                        cssClassForStatus={cssClassForStatus}
	                        cssClassForStatusIcon={cssClassForStatusIcon}
	                    />
	                }/>
	                <Route path='/profile/history/purchase' render={(props) =>
	                    <HistoryPurchaseView
	                        operations={purchaseOperations}
	                        cssClassForStatus={cssClassForStatus}
	                        cssClassForStatusIcon={cssClassForStatusIcon}
	                    />
	                }/>
	                <Route path='/profile/history/withdraw' render={(props) =>
	                    <HistoryWithdrawView
	                        operations={withdrawOperations}
	                        cssClassForStatus={cssClassForStatus}
	                        cssClassForStatusIcon={cssClassForStatusIcon}
	                    />
	                }/>
	                <Route path='/profile/withdraw' render={() =>
	                    <ProfileWithdraw profile={this.props.profile}/>
	                }/>
		            <Route path='/profile/feedback' component={Feedback}/>
		            <Route path='/profile/favorites' component={ProfileFavorites}/>
	            </Switch>
            </ProfileWrap>
        );
    }
}

export default connect(
    state => ({
        profile: state.profileState.data,
        operations: state.operationsState.data
    })
)(Profile)
