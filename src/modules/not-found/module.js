import React, { PureComponent } from "react"
import { Switch, Route, Link, withRouter } from 'react-router-dom'

class NotFound extends  PureComponent {

	render(){

		return (
			<div className="">
				<h1 style={{marginTop: '40px'}}>Такой страницы не существует</h1>
				<Link  className="back-to-main" to=''>Перейти на главную</Link>
			</div>
		)
	}
}

export default withRouter(NotFound)