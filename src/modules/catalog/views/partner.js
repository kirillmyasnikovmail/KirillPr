
import React from 'react'
import { Link } from 'react-router-dom';

const CatalogShop = props => {
    const shop = props.shop;
    // Скролл вверх
    parent.postMessage({top: true}, '*')
    return(
        <div className="offer">
            <a href="#" className="btn btn--back" onClick={()=>window.history.back()}>НАЗАД</a>
            <h1 className="page-title">Покупки с бонусами в {shop.brand.name}</h1>
                <div className="blc offer__blc">
                    <div className="offer__top-bar">
                        <button className={"btn fav offer__fav" + (props.isInFavorites(shop.id) ? " active" : "")} title="В избранное" onClick={props.addToFavorites.bind(null, shop.id)}></button>
                        <div className="offer__logo">
                            <img src={shop._src} alt="" />
                        </div>
                    </div>  
                    {shop.promoActions[0].amount.type == "B" ?
                        <div className="offer__cashback">
                            Баллы
                            <b>до {shop.promoActions[0].amount.value}</b>
                        </div>
                        :
                        <div className="offer__cashback">
                            Бонусов до
                            <b>до {shop.promoActions[0].amount.value}%</b>
                        </div>
                    }
                    <div className="icns clearfix">
                        <div className="icn icn--sandclock">30 дней —  среднее время ожидания баллов</div>
                        <div className="icn icn--i">Перед совершением покупок ознакомьтесь с <Link style={{color: 'rgba(146, 146, 146, 0.87)', textDecoration: 'underline'}} to='/faq'>правилами покупок</Link> с баллами.</div>
                    </div>
                    <a target='_blank' href={shop._partnerExternalLink} className="btn btn--blue offer__go-in-mart">Перейти в магазин</a>
                    <h2 className="offer__blc-title">Как получить бонусы?</h2>
                    <div className="offer__steps">
                        <div className="offer__step">
                            <div className="offer__step-number">1</div>
                            <div className="offer__step-txt">Перейдите на сайт</div>
                        </div>
                        <div className="offer__step">
                            <div className="offer__step-number">2</div>
                            <div className="offer__step-txt">Оплатите заказ удобным способом</div>
                        </div>
                        <div className="offer__step">
                            <div className="offer__step-number">3</div>
                            <div className="offer__step-txt">Получите бонусы на свой счет с каждой покупки</div>
                        </div>
                    </div>
                </div>
                <article className="offer__article">
                    <p dangerouslySetInnerHTML={{__html: shop.descriptionHtml}} />
                    <h3>Условия получения бонусных рублей</h3>
                    <div dangerouslySetInnerHTML={{__html: shop.promoActions[0].descriptionHtml}} />
                </article>
            </div>
    )
}

export default CatalogShop;