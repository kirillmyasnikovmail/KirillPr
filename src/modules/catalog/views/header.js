import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Actions from '../../../redux/action-creators'
import { NavLink, withRouter } from 'react-router-dom';

class CatalogHeader extends PureComponent {
    constructor(props) {
        super(props)
        this.sort = window.location.pathname.split('/')[2]
    }

    componentWillUnmount () {
        this.props.actions.changeSearchText('')
    }

    // диспатчим изменения сортировки
    changeSortType (e, type) {
        if (this.sort === 'categories') {
            this.props.actions.changeSortType(type)

            // запращаем всплывать событию
            e.stopPropagation()
            e.preventDefault()
            return false
        }
    }

    hideCategoriesMenu () {
        // удаляем поисковой запрос
        this.props.actions.changeSearchText('')

        const menu = document.querySelector('.categories-menu')
        menu.classList.add('hide')
        setTimeout(() => menu.classList.remove('hide'), 300)
    }

    render() {
        this.sort = window.location.pathname.split('/')[2]
        const sort = this.sort
        const type = this.props.SORT_TYPE
        const categories = this.props.categories.map((item, index) => 
            <li key={index}>
                <span>
                    <NavLink to={'/partners/categories/' + item._name + '/popular'} activeClassName='active' onClick={() => this.hideCategoriesMenu()}>
                        {item.name} <span>{item.count}</span>
                    </NavLink>
                </span>
            </li>
        )
        const sortVariables = {
            'popular': 'популярности',
            'az': 'алфавиту',
            'latest': 'новизне'
        }
        
        return (
            <div>
                <div className='container'>
                    <div className='top-bar clearfix'>
                        <div className="filter">
                            {/*Категории*/}
                            <div className="select-wrap">

                                <div className="cs-select select-gray filter-catalog" tabIndex="0">
                                    <span className="cs-placeholder">Все категории</span>
                                    <div className="cs-options">
                                        <ul>
                                            <li><span>
                                                <NavLink to='/partners/popular' activeClassName='active'>Все категории <span>{this.props.count}</span></NavLink>
                                            </span></li>
		                                    {categories}
                                        </ul>
                                     </div>
                                </div>
                            </div>
                            {/*Сортировка*/}
                            <div className="select-wrap select-wrap-sort">
                                <div className="cs-select select-gray filter-sort" tabIndex="0">
                                    <span className="cs-placeholder">Сортировать по популярности</span>
                                    <div className="cs-options">
                                        <ul>
                                            <li>
                                                <span>
                                                    <NavLink
                                                        to={'/partners/popular'}
                                                        activeClassName='active'
                                                        onClick={(e) => this.changeSortType(e, 'popular')}
                                                        className={(sort === 'categories' ? (this.props.sortType === 'popular' ? 'cs-selected' : '') : '')}
                                                    >
                                                        По популярности
                                                    </NavLink>
                                                </span>
                                            </li>
                                            <li>
                                                <span>
                                                    <NavLink
                                                        to={'/partners/az'}
                                                        activeClassName='active'
                                                        onClick={(e) => this.changeSortType(e, 'az')}
                                                        className={(sort === 'categories' ? (this.props.sortType === 'az' ? 'cs-selected' : '') : '')}
                                                    >
                                                        По алфавиту
                                                    </NavLink>
                                                </span>
                                            </li>
                                            <li>
                                                <span>
                                                    <NavLink
                                                        to={'/partners/latest'}
                                                        activeClassName='active'
                                                        onClick={(e) => this.changeSortType(e, 'latest')}
                                                        className={(sort === 'categories' ? (this.props.sortType === 'latest' ? 'cs-selected' : '') : '')}
                                                    >
                                                        По новизне
                                                    </NavLink>
                                                </span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {/*Поиск*/}
                            <form className="filter-search" action="" method="post">
                                <input type='search' defaultValue={this.props.searchText} onChange={(e) => this.props.actions.changeSearchText(e.target.value)} className='input input-gray input-dropdown input-filter-search' name='search' placeholder='Поиск партнеров c бонусами'/>
                                    <span className="input-filter-submit"/>
                            </form>
                        </div>

                        {/**/}
                        {/*<div className='top-bar-links clearfix'>
                         <div className='top-bar-link-blc'>
                         <NavLink to='/partners/popular' className='top-bar-link'>Все предложения</NavLink>
                         </div>
                         <div className='top-bar-link-blc top-bar-link-blc--drop'>
                         <a href='#' className='top-bar-link'>Категории</a>
                         <div className='top-bar-drop top-bar-drop--big categories-menu'>
                         <ul>
                         <li><NavLink to='/partners/popular' activeClassName='active'>Все категории <span>{this.props.count}</span></NavLink></li>
                         {categories}
                         </ul>
                         </div>
                         </div>
                         <div className='top-bar-link-blc top-bar-link-blc--drop top-bar-link-blc--filter'>
                         <a href='#' className='top-bar-link'>
                         Сортировать по
                         {this.sort === 'categories'
                         ? sortVariables.hasOwnProperty(this.props.sortType) ? sortVariables[this.props.sortType] : 'популярности'
                         : sortVariables.hasOwnProperty(sort) ? sortVariables[sort] : 'популярности'
                         }
                         </a>
                         <div className='top-bar-drop top-bar-drop--filter'>
                         <ul>
                         <li>
                         <NavLink
                         to={'/partners/popular'}
                         activeClassName='active'
                         onClick={(e) => this.changeSortType(e, 'popular')}
                         className={(sort === 'categories' ? (this.props.sortType === 'popular' ? 'active' : '') : '')}
                         >
                         По популярности
                         </NavLink>
                         </li>
                         <li>
                         <NavLink
                         to={'/partners/az'}
                         activeClassName='active'
                         onClick={(e) => this.changeSortType(e, 'az')}
                         className={(sort === 'categories' ? (this.props.sortType === 'az' ? 'active' : '') : '')}
                         >
                         По алфавиту
                         </NavLink>
                         </li>
                         <li>
                         <NavLink
                         to={'/partners/latest'}
                         activeClassName='active'
                         onClick={(e) => this.changeSortType(e, 'latest')}
                         className={(sort === 'categories' ? (this.props.sortType === 'latest' ? 'active' : '') : '')}
                         >
                         По новизне
                         </NavLink>
                         </li>
                         </ul>
                         </div>
                         </div>
                         </div>
                         <div className='search'>
                         <input type='search' defaultValue={this.props.searchText} onChange={(e) => this.props.actions.changeSearchText(e.target.value)} className='input input--search' name='search' placeholder='Поиск партнеров c бонусами'/>
                         </div>*/}
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(connect(
    state => ({
        sortType: state.catalogState.sort,
        categories: state.catalogState.categories,
        searchText: state.catalogState.searchText,
        count: state.catalogState.data.length
    }),
    dispatch => ({
        actions: bindActionCreators(Actions, dispatch)
    })
)(CatalogHeader))
