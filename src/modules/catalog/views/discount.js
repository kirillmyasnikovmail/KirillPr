/**
 * Created by diman on 14.06.17.
 */

import React from 'react';
import { Link } from 'react-router-dom';


const DiscountList = props => {
    return(
        <div className="frame__content">
            <div className="container">
                <div className="page-title">
                    <div className="container">
                        <Link to="/partners"><div className="page-title__arrow"></div>Назад в каталог</Link>
                    </div>
                </div>
                <h1>Товары со скидкой</h1>
            </div>
        </div>
    )
}
export default DiscountList