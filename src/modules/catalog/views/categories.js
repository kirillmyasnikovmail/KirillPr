/**
 * Created by diman on 14.06.17.
 */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

class CategoryList extends PureComponent {
  render () {
    const items = this.props.categories.map((item, index) =>
      <div key={index} className="row__col-xs-2 ib">
        <Link to={"/partners/" + props.SORT_TYPE.CATEGORIES +"/" + item._name + '/' + 'popular'} className="frame-category">
          <div className="frame-category__image"><img src={item.src}/></div>
          <span>{item.name} ({item.count})</span>
        </Link>
      </div>
    )
    return (
      <div className="frame__content">
        <div className="container">
          <div className="page-title">
            <div className="container">
              <Link to="/partners"><div className="page-title__arrow"></div>Назад в каталог</Link>
            </div>
          </div>
          <div className="row ib-parent">
            <div className="row__col-xs-2 ib">
              <Link to="/partners" className="frame-category">
                <div className="frame-category__image frame-category__image--all"></div>
                <span>Все ({this.props.categories.length})</span>
              </Link>
            </div>
            {items}
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({categories: state.catalogState.categories})
)(CategoryList)
