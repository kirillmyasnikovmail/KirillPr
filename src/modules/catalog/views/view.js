import React, {PureComponent} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import ExtBanner from './banner'
import { Carousel } from 'react-responsive-carousel';


class CatalogList extends PureComponent {
	constructor(props) {
		super(props)
		this.state = {
			offerOnPage: 75,
			offersOnPageStep: 60
		}
	}

	render() {
		const items = this.props.catalog.filter((item, i) => i <= this.state.offerOnPage).map((item, index) =>
			<div className="catalog-brands__item" key={index}>
				<Link to={item._partnerLink} className='catalog-brands__link'>
					<div className="catalog-brands__block-img">
						<img className="catalog-brands__img" src={item._src} />
					</div>
					<div className="catalog-brands__info">
						{item.promoActions.length > 1
							? <div className='catalog-brands__price-old'>До <span
								className='catalog-brands__price-old-number'>{item.promoActions[1].amount.value}{item.promoActions[1].amount.type}</span>
							</div>
							: <div className='item__txt'>
								До <div className='item__persent'>{item.promoActions[0].amount.value}{item.promoActions[0].amount.type === '%' ? '%' : ''}</div>
							</div>
						}
						{/*<div  className="add-favorites_icon"></div>*/}
					</div>
				</Link>
				<div onClick={(e) => this.props.addToFavorites(item.id, e)} className="catalog-brands-favorites"><a className="catalog-brands-favorites-wrap" href="#"></a></div>
				{/*<div key={index} className='item'>
				 <Link to={item._partnerLink} className='item__link'>
				 <div className='item__img'>
				 <img src={item._src} alt='mediamarkt'/>
				 </div>
				 {item.promoActions.length > 1
				 ? <div className='item__txt'>Бонусов до <span
				 className='through'>{item.promoActions[1].amount.value}{item.promoActions[1].amount.type}</span>
				 </div>
				 : <div className='item__txt'>Бонусов до</div>
				 }
				 <div
				 className='item__persent'>{item.promoActions[0].amount.value}{item.promoActions[0].amount.type === '%' ? '%' : ''}</div>
				 </Link>
				 <button onClick={(e) => this.props.addToFavorites(item.id, e)}
				 className={`btn fav ${this.props.isInFavorites(item.id) ? 'active' : ''}`}
				 title='В избранное'></button>
				 </div>*/}
			</div>
		)

		return (
			<div>
				<div className="slider" >
					{/*https://www.npmjs.com/package/react-responsive-carousel*/}
					<Carousel axis="horizontal" showThumbs={false} showArrows={true} dynamicHeight={false} emulateTouch>
						<div>
							<a href="/partners/lamoda">
								<img src="/images/slider/lamoda_300817.1910x200.png" />
							</a>
							{/*<p className="legend">Lamoda</p>*/}
						</div>
						<div>
							<a href="/partners/oldi">
								<img src="/images/slider/oldi_300817.910x200.png" />
							</a>
							{/*<p className="legend">Oldi</p>*/}
						</div>
						<div>
							<a href="/partners/rcmoment">
								<img src="/images/slider/rcmoment_300817.910x200.png" />
							</a>
							{/*<p className="legend">RCmoment</p>*/}
						</div>
						<div>
							<a href="/partners/sport-point">
								<img src="/images/slider/sportpoint_300817.910x200.png" />
							</a>
							{/*<p className="legend">Sport point</p>*/}
						</div>
						<div>
							<a href="/partners/tefal">
								<img src="/images/slider/tefal_300817.910x200.png" />
							</a>
							{/*<p className="legend">Tefal</p>*/}
						</div>
					</Carousel>
				</div>
				<ExtBanner/>
				<div className="items catalog-brands profile-design">
					<div className="items__wrap">
						{items}
					</div>
					{this.state.offerOnPage <= this.props.catalog.length
						? <button style={{margin: '15px auto'}} className="btn-color-green" onClick={() => {
							this.setState({offerOnPage: this.state.offerOnPage + this.state.offersOnPageStep})
							setTimeout(() => {
								parent.postMessage({height: document.getElementById("main").scrollHeight}, "*");
							}, 1000)
						}}>Загрузить еще</button>
						: null
					}
				</div>
			</div>
		)
	}
}

export default connect(
	state => ({})
)(CatalogList)
