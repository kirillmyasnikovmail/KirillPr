import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
const EXT_ID = "hpkajobmmfgnmmljdoajmkmohgjcanan"

class ExtBanner extends PureComponent {
    constructor(props){
        super(props)
        this.state = {
            showBanner: true
        }
    }

    componentWillMount() {
        if(typeof chrome != "undefined" && chrome.runtime){
            const self = this
            chrome.runtime.sendMessage(EXT_ID, {message: "version"}, function (reply) {
                self.setState({showBanner: !reply})
            });
        }
    }
    
    render() {
        const path = window.location.pathname.split('/')
        if (path[1] !== 'partners') {
            return null
        }
        
        // Скрываем банер на странице офера
        const findOffer = this.props.catalog.find(item => item._partnerLink.split('/')[2] === path[2])
        if (findOffer) {
            return null
        }
        console.log('поиск офера')

        const extensionLink = `https://chrome.google.com/webstore/detail/workle-%D0%B1%D0%BE%D0%BD%D1%83%D1%81/${EXT_ID}?hl=ru`
        if (this.state.showBanner) {
             return (
               <div className="banner">
                 <a href={extensionLink} target="_blank"><img src="/images/example/baner-catalog.png" alt=""/></a>
               </div>
            )
        } else {
            return null
        }
    }
}

export default connect(
    state => ({catalog: state.catalogState.data})
)(ExtBanner)
