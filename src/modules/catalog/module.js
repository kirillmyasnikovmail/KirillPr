import React from 'react'
import { connect } from 'react-redux'
import Cookies from 'universal-cookie'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import _ from 'underscore'
import * as helpers from "../../helpers"
import CatalogList from './views/view'
import CatalogShop from './views/partner'
import CategoryList from './views/categories'
import DiscountList from './views/discount'

const cookies = new Cookies()
if (!cookies.get("favorites")){
  cookies.set("favorites", "", {path: '/'})
}

const SORT_TYPE = {
  POPULAR: "popular",
  NAME: "az",
  LATEST: "latest",
  FAVORITES: "favorites",
  CATEGORIES: "categories",
  DISCOUNT: "discount",
}

class Catalog extends React.Component{
  constructor(props){
    super(props)
    this.stateChange = this.stateChange.bind(this)
    this.addToFavorites = this.addToFavorites.bind(this)
    this.isInFavorites = this.isInFavorites.bind(this)
    this.loadMoreOffers = this.loadMoreOffers.bind(this)
    this.state = {
      searchText: "",
      sort: SORT_TYPE.POPULAR,
      offersOnPage: 30,
      offersOnPageStep: 15,
    }
  }

  componentDidMount(){
    // document.addEventListener("scroll", _.throttle(this.loadMoreOffers, 300))
  }

  loadMoreOffers(){
    const height = document.body.scrollHeight - window.pageYOffset - document.body.offsetHeight
    if(height < 200){
      const offersOnPage = this.state.offersOnPage + this.state.offersOnPageStep
      this.setState({offersOnPage: offersOnPage})
    }
  }

  

  addToFavorites(id, e){
    const favorites = cookies.get("favorites").split(','),
      index = favorites.indexOf(id)
    if(index == -1){
      favorites.push(id)
    }
    else{
      favorites.splice(index, 1)
    }
    cookies.set("favorites", favorites.join(","), {path: '/'})
    this.forceUpdate()
    e.preventDefault()
  }

  isInFavorites(id){
    const favorites = cookies.get("favorites").split(',')
    if(favorites.indexOf(id) !== -1){
      return true
    }
    return false
  }

  stateChange(e){
    const state = {}
    state[e.target.name] = e.target.value
    this.setState(state)
  }

  addCategory(categories, item){
    var index = categories.findIndex(category => category.name == item.name)
    if(index == -1){
      categories.push({
        name: item.name,
        _name: item._name,
        src: "https://payqr.ru" + item.imageUrl,
        count: 1,
      })
    }
    else{
      categories[index].count++
    }
  }

  render(){
    const baseUrl = window.location.pathname.split("/")[1]
    let shop, catalog, categories = []
    const sort = window.location.pathname.split("/")[2] ? decodeURI(window.location.pathname.split("/")[2]) : SORT_TYPE.POPULAR
    if(this.props.catalog.length > 0){
      catalog = this.props.catalog.slice()
      catalog.map(item => {
        const shopSemanticUrl = helpers.getSemanticLatinName(item.name)
        if(sort == shopSemanticUrl || sort == item.id){
          shop = item
        }
        this.addCategory(categories, item.category)
      })

      if(this.props.searchText){
        catalog = catalog.filter(item => {
          const latinName = helpers.getSemanticLatinName(item.name),
            cyrillicName = helpers.getSemanticCyrillic(item.name),
            search = this.props.searchText.toLowerCase()
          if(latinName.indexOf(search) != -1 || cyrillicName.indexOf(search) != -1){
            return item
          }
        })
      }

      return (
        <div>
          <Switch>
            <Route path={`/${baseUrl}/` + SORT_TYPE.CATEGORIES + "/:categoryName/:sort"} render={(props) => {
              catalog = catalog.filter(item => item.category._name === props.match.params.categoryName)

              // сортировка в категориях
              switch (this.props.sortType) {
                case 'popular':
                  catalog.sort((curr, next)=>{
                    if (next.label > curr.label) return 1
                    if (next.label < curr.label) return -1
                    if (next.rating > curr.rating) return 1
                    if (next.rating < curr.rating) return -1
                    return 0
                  })
                  break

                case 'az':
                  catalog.sort((curr, next) => curr.name.toUpperCase() < next.name.toUpperCase() ? -1 : 1)
                  break

                case 'latest':
                  // Данные поумолчанию отсортированы по новизне
                  break
                default:
                  // Ничего не сортируем
                  break
              }

              return (
                <CatalogList
                  catalog={catalog}
                  addToFavorites={this.addToFavorites}
                  isInFavorites={this.isInFavorites}
                  sort={sort}
                  SORT_TYPE={SORT_TYPE}
                />
              )}
            }/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.CATEGORIES} component={CategoryList}/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.DISCOUNT} render={() => (
              <DiscountList/>
            )}/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.POPULAR} render={() => {
              catalog.sort((curr, next) => next.rating - curr.rating)
              //catalog = catalog.splice(0, this.state.offersOnPage)
              return (
                <CatalogList stateChange={this.stateChange} catalog={catalog} addToFavorites={this.addToFavorites} isInFavorites={this.isInFavorites} sort={sort} SORT_TYPE={SORT_TYPE}/>
              )}
            }/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.NAME} render={() => {
              catalog.sort((curr, next) => curr.name.toUpperCase() < next.name.toUpperCase() ? -1 : 1)
              return (
                <CatalogList stateChange={this.stateChange} catalog={catalog} addToFavorites={this.addToFavorites} isInFavorites={this.isInFavorites} sort={sort} SORT_TYPE={SORT_TYPE}/>
              )}
            }/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.LATEST} render={() => {
              return (
                <CatalogList stateChange={this.stateChange} catalog={catalog} addToFavorites={this.addToFavorites} isInFavorites={this.isInFavorites} sort={sort} SORT_TYPE={SORT_TYPE}/>
              )}
            }/>
            <Route path={`/${baseUrl}/` + SORT_TYPE.FAVORITES} render={() => {
              catalog = catalog.filter(item => {
                if(this.isInFavorites(item.id)){
                  return item
                }
              })
              setTimeout(()=>{
                  parent.postMessage({height: (document.getElementById("main").scrollHeight + 500)}, "*");
              }, 1000)
              return (
                <CatalogList stateChange={this.stateChange} catalog={catalog} addToFavorites={this.addToFavorites} isInFavorites={this.isInFavorites} sort={sort} SORT_TYPE={SORT_TYPE}/>
              )}
            }/>
            <Route path={`/${baseUrl}/:shopName`} render={() => (
              <CatalogShop shop={shop} addToFavorites={this.addToFavorites} isInFavorites={this.isInFavorites}/>
            )}/>
            <Redirect to={`/${baseUrl}/popular`}/>
          </Switch>

        </div>
      )
    }
    return(
      <img id="preloader" src="/assets/img/preloader.gif"/>
    )
  }
}

export default withRouter(connect(
  state => ({
    catalog: state.catalogState.data,
    searchText: state.catalogState.searchText,
    sortType: state.catalogState.sort
  })
)(Catalog))
